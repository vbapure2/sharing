﻿using backendproject.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace backendproject.Repository
{
    public class MemberRepository
    {
        public static string GetEncryptedKey(string key)
        {
            string encryptionKey = Config.MemberPasswordEncryptionKey;
            string encryptionIV = Config.MemberPasswordEncryptionIV;
            return Convert.ToBase64String(SecurityUtils.TripleDESEncrypt(Encoding.ASCII.GetBytes(key), Encoding.ASCII.GetBytes(encryptionKey), Encoding.ASCII.GetBytes(encryptionIV)));
        }

        public static string GenerateSalt()
        {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray());
        }

        public static string GetPasswordHash(string password, string salt)
        {
            string encryptedPass = MakePassword(password, salt);

            string passwordHashToCompare = SecurityUtils.GetMD5Hash(Encoding.ASCII.GetBytes(encryptedPass));
            return passwordHashToCompare;
        }

        // Password Hash stored in the database
        public static string MakePassword(string password, string salt)
        {
            string encryptionKey = Config.MemberPasswordEncryptionKey;
            string encryptionIV = Config.MemberPasswordEncryptionIV;

            if (string.IsNullOrEmpty(encryptionKey))
                throw new Exception("Encryption key not defined");
            if (string.IsNullOrEmpty(encryptionIV))
                throw new Exception("Encryption IV not defined");

            string encodedPassword = Convert.ToBase64String(
                    SecurityUtils.TripleDESEncrypt(
                        Encoding.ASCII.GetBytes(string.Format("{0}{1}", password, salt)), Encoding.ASCII.GetBytes(encryptionKey), Encoding.ASCII.GetBytes(encryptionIV)));
            return encodedPassword;
        }
    }
}
