﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Forwarder.Portal.Lending.WizardPay.Customer.Middleware
{
    public class LogHeadersMiddleware
    {
        private readonly RequestDelegate next;
        public static readonly List<string> RequestHeaders = new List<string>();
        public static readonly List<string> ResponseHeaders = new List<string>();

        public LogHeadersMiddleware(RequestDelegate next)
        {
            this.next = next;
            // this._logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var uniqueRequestHeaders = context.Request.Headers;
                //.Where(x => RequestHeaders.All(r => r != x.Key))
                //.Select(x => x.Key);
            // RequestHeaders.AddRange(uniqueRequestHeaders);
            // _logger.Log(1, $"Request Headers: {JsonConvert.SerializeObject(uniqueRequestHeaders)}");
            // this._logger.LogInformation(string.Format("Logging Middleware: {0}", JsonConvert.SerializeObject(uniqueRequestHeaders)));
            
            await this.next.Invoke(context);
            var uniqueResponseHeaders = context.Response.Headers;
                //.Where(x => ResponseHeaders.All(r => r != x.Key))
                //.Select(x => x.Key);
            // ResponseHeaders.AddRange(uniqueResponseHeaders);
            // _logger.Log(1, $"Response Headers: {JsonConvert.SerializeObject(uniqueResponseHeaders)}");
        }
    }
}
