﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace backendproject.Common
{
    public class CommonUtils
    {
        public static List<string> GetModelStateErrors(ModelStateDictionary modelState)
        {
            var modelKeys = modelState.Keys;
            List<string> errors = new List<string>();
            foreach (string key in modelKeys)
            {
                var fieldErrors = modelState[key].Errors.Select(x => string.IsNullOrWhiteSpace(key) == false
                    ? key + ": " + x.ErrorMessage
                    : x.ErrorMessage
                ).ToList();

                errors.AddRange(fieldErrors);
            }
            return errors;
        }

        public static JsonResult StandardizedJsonResponse(ModelStateDictionary modelState, dynamic data = null, HttpStatusCode status = HttpStatusCode.OK)
        {
            var message = GetModelStateErrors(modelState);
            return new JsonResult(new
            {
                status = (int)status,
                message = message,
                data = data
            }, status);
        }

        public static JsonResult StandardizedJsonResponse(string message, dynamic data = null, HttpStatusCode status = HttpStatusCode.OK)
        {
            return new JsonResult(new
            {
                status = (int)status,
                message = message,
                data = data
            }, status);
        }

        public static JsonResult StandardizedJsonResponse(IEnumerable<string[]> message, dynamic data = null, HttpStatusCode status = HttpStatusCode.OK)
        {
            return new JsonResult(new
            {
                status = (int)status,
                message = message,
                data = data
            }, status);
        }
    }
}
