﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace backendproject.Common
{
    public class Config
    {
        private static IConfigurationRoot _configuration { get; set; }

        static Config()
        {
            var _builder = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            _configuration = _builder.Build();

        }

        public static string Key
        {
            get
            {
                return _configuration.GetSection("Settings:Key").Value;
            }
        }

        public static string PasswordExpiration
        {
            get
            {
                return _configuration.GetSection("Settings:PasswordExpiration").Value;
            }
        }

        public static string TokenExpiration
        {
            get
            {
                return _configuration.GetSection("Settings:TokenExpiration").Value;
            }
        }

        public static string Issuer
        {
            get
            {
                return _configuration.GetSection("Settings:Issuer").Value;
            }
        }

        public static string MemberPasswordEncryptionKey
        {
            get
            {

                return _configuration.GetSection("Settings:MemberPasswordEncryptionKey").Value;
            }
        }

        public static string MemberPasswordEncryptionIV
        {
            get
            {

                return _configuration.GetSection("Settings:MemberPasswordEncryptionIV").Value;
            }
        }

        public static string EMAIL_Host
        {
            get
            {

                return _configuration.GetSection("EmailSettings:Host").Value;
            }
        }

        public static string EMAIL_FromAddress
        {
            get
            {

                return _configuration.GetSection("EmailSettings:FromAddress").Value;
            }
        }

    }
}
