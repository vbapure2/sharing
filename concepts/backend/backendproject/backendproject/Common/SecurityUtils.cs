﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace backendproject.Common
{
    public class SecurityUtils
    {
        private static byte[] TRIPLE_DES_KEY = new byte[] { 12, 44, 90, 13, 65, 171, 153, 71, 10, 189, 245, 55, 29, 255, 23, 43, 70, 201, 112, 12, 88, 32, 1, 40 };
        private static byte[] TRIPLE_DES_IV = new byte[] { 182, 191, 43, 17, 89, 19, 31, 208 };

        public static string Encrypt(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                byte[] arr = ASCIIEncoding.ASCII.GetBytes(input);

                byte[] encrypted = TripleDESEncrypt(arr);

                return Convert.ToBase64String(encrypted);
            }
            else
            {
                return "";
            }
        }

        public static string Decrypt(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                byte[] arr = Convert.FromBase64String(input);

                byte[] decrypted = TripleDESDecrypt(arr);

                return ASCIIEncoding.ASCII.GetString(decrypted);
            }
            else
            {
                return "";
            }
        }

        public static byte[] TripleDESEncrypt(string input, byte[] key, byte[] initializationVector)
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);

            return TripleDESEncrypt(inputBytes, key, initializationVector);
        }

        public static byte[] TripleDESEncrypt(byte[] inputBytes)
        {
            return TripleDESEncrypt(inputBytes, TRIPLE_DES_KEY, TRIPLE_DES_IV);
        }

        // uses the default encryption key and IV to encrypt
        public static byte[] TripleDESEncrypt(byte[] inputBytes, byte[] key, byte[] initializationVector)
        {
            byte[] result = null;

            using (TripleDESCryptoServiceProvider e = new TripleDESCryptoServiceProvider())
            {
                using (ICryptoTransform ct = e.CreateEncryptor(key, initializationVector))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cStream = new CryptoStream(ms, ct, CryptoStreamMode.Write))
                        {
                            cStream.Write(inputBytes, 0, inputBytes.Length);
                            cStream.FlushFinalBlock();

                            ms.Position = 0;
                            int len = (int)ms.Length;
                            result = new byte[len];
                            ms.Read(result, 0, len);
                        }
                    }
                }
            }

            return result;
        }

        public static byte[] TripleDESDecrypt(string input, byte[] key, byte[] initializationVector)
        {
            byte[] inputBytes = Convert.FromBase64String(input);

            return TripleDESDecrypt(inputBytes, key, initializationVector);
        }

        public static byte[] TripleDESDecrypt(byte[] inputBytes)
        {
            return TripleDESDecrypt(inputBytes, TRIPLE_DES_KEY, TRIPLE_DES_IV);
        }

        public static byte[] TripleDESDecrypt(byte[] inputBytes, byte[] key, byte[] initializationVector)
        {
            byte[] result = null;

            using (TripleDESCryptoServiceProvider e = new TripleDESCryptoServiceProvider())
            {
                using (ICryptoTransform ct = e.CreateDecryptor(key, initializationVector))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cStream = new CryptoStream(ms, ct, CryptoStreamMode.Write))
                        {
                            cStream.Write(inputBytes, 0, inputBytes.Length);
                            cStream.FlushFinalBlock();

                            ms.Position = 0;
                            int len = (int)ms.Length;
                            result = new byte[len];
                            ms.Read(result, 0, len);
                        }
                    }
                }
            }

            return result;
        }

        public static string GetMD5Hash(byte[] fileBytes)
        {
            byte[] hash = null;
            using (MD5 md5 = new MD5CryptoServiceProvider())
            {
                hash = md5.ComputeHash(fileBytes, 0, fileBytes.Length);
            }

            StringBuilder sb = new StringBuilder();
            if (hash != null)
            {
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
            }
            return sb.ToString();
        }

        public static string GetMD5Hash(string str)
        {
            return GetMD5Hash(System.Text.Encoding.ASCII.GetBytes(str));
        }

        public static string GetMD5HashBase64(string str)
        {
            return GetMD5Hash(System.Text.Encoding.ASCII.GetBytes(Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(str))));
        }

        public static string EncodeBase64String(string str)
        {
            return Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(str));
        }

        public static string DecodeBase64String(string str)
        {
            return System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(str));
        }

        public static string ExportPublicKey(RSACryptoServiceProvider csp)
        {
            StringWriter outputStream = new StringWriter();
            var parameters = csp.ExportParameters(false);
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write((byte)0x30); // SEQUENCE
                using (var innerStream = new MemoryStream())
                {
                    var innerWriter = new BinaryWriter(innerStream);
                    innerWriter.Write((byte)0x30); // SEQUENCE
                    EncodeLength(innerWriter, 13);
                    innerWriter.Write((byte)0x06); // OBJECT IDENTIFIER
                    var rsaEncryptionOid = new byte[] { 0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x01 };
                    EncodeLength(innerWriter, rsaEncryptionOid.Length);
                    innerWriter.Write(rsaEncryptionOid);
                    innerWriter.Write((byte)0x05); // NULL
                    EncodeLength(innerWriter, 0);
                    innerWriter.Write((byte)0x03); // BIT STRING
                    using (var bitStringStream = new MemoryStream())
                    {
                        var bitStringWriter = new BinaryWriter(bitStringStream);
                        bitStringWriter.Write((byte)0x00); // # of unused bits
                        bitStringWriter.Write((byte)0x30); // SEQUENCE
                        using (var paramsStream = new MemoryStream())
                        {
                            var paramsWriter = new BinaryWriter(paramsStream);
                            EncodeIntegerBigEndian(paramsWriter, parameters.Modulus); // Modulus
                            EncodeIntegerBigEndian(paramsWriter, parameters.Exponent); // Exponent
                            var paramsLength = (int)paramsStream.Length;
                            EncodeLength(bitStringWriter, paramsLength);
                            bitStringWriter.Write(paramsStream.GetBuffer(), 0, paramsLength);
                        }
                        var bitStringLength = (int)bitStringStream.Length;
                        EncodeLength(innerWriter, bitStringLength);
                        innerWriter.Write(bitStringStream.GetBuffer(), 0, bitStringLength);
                    }
                    var length = (int)innerStream.Length;
                    EncodeLength(writer, length);
                    writer.Write(innerStream.GetBuffer(), 0, length);
                }

                var base64 = Convert.ToBase64String(stream.GetBuffer(), 0, (int)stream.Length).ToCharArray();
                // WriteLine terminates with \r\n, we want only \n
                outputStream.Write("-----BEGIN PUBLIC KEY-----\n");
                for (var i = 0; i < base64.Length; i += 64)
                {
                    outputStream.Write(base64, i, Math.Min(64, base64.Length - i));
                    outputStream.Write("\n");
                }
                outputStream.Write("-----END PUBLIC KEY-----");
            }

            return outputStream.ToString();
        }

        private static void EncodeLength(BinaryWriter stream, int length)
        {
            if (length < 0) throw new ArgumentOutOfRangeException("length", "Length must be non-negative");
            if (length < 0x80)
            {
                // Short form
                stream.Write((byte)length);
            }
            else
            {
                // Long form
                var temp = length;
                var bytesRequired = 0;
                while (temp > 0)
                {
                    temp >>= 8;
                    bytesRequired++;
                }
                stream.Write((byte)(bytesRequired | 0x80));
                for (var i = bytesRequired - 1; i >= 0; i--)
                {
                    stream.Write((byte)(length >> (8 * i) & 0xff));
                }
            }
        }

        /// <summary>
        /// https://stackoverflow.com/a/23739932/2860309
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="value"></param>
        /// <param name="forceUnsigned"></param>
        private static void EncodeIntegerBigEndian(BinaryWriter stream, byte[] value, bool forceUnsigned = true)
        {
            stream.Write((byte)0x02); // INTEGER
            var prefixZeros = 0;
            for (var i = 0; i < value.Length; i++)
            {
                if (value[i] != 0) break;
                prefixZeros++;
            }
            if (value.Length - prefixZeros == 0)
            {
                EncodeLength(stream, 1);
                stream.Write((byte)0);
            }
            else
            {
                if (forceUnsigned && value[prefixZeros] > 0x7f)
                {
                    // Add a prefix zero to force unsigned if the MSB is 1
                    EncodeLength(stream, value.Length - prefixZeros + 1);
                    stream.Write((byte)0);
                }
                else
                {
                    EncodeLength(stream, value.Length - prefixZeros);
                }
                for (var i = prefixZeros; i < value.Length; i++)
                {
                    stream.Write(value[i]);
                }
            }
        }
    }
}
