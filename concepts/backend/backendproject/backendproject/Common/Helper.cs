﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace backendproject.Common
{
    public static class Helper
    {
        public static void SendEmail(string toEmail, string body)
        {
            SmtpClient client = new SmtpClient(Config.EMAIL_Host);
            client.EnableSsl = false;

            MailAddress from = new MailAddress(Config.EMAIL_FromAddress);
            MailAddress to = new MailAddress(toEmail);

            MailMessage mailMessage = new MailMessage(from, to);

            // template message
            mailMessage.Body = body;
            client.Send(mailMessage);
        }
    }
}
