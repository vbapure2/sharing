﻿using backendproject.Data.Entities;
using backendproject.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backendproject.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<LoginModel> LoginModels { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<LoginModel>().HasData(new LoginModel
            {
                Id = 1,
                Username = "johndoe",
                Password = "def@123"
            });

            base.OnModelCreating(builder);
        }
    }
}
