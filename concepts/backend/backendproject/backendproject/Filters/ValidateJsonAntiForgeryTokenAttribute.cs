﻿using backendproject.Common;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Helpers;

namespace backendproject.Filters
{
    public class ValidateJsonAntiForgeryTokenAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Serilog.Log.Logger.Information("ValidateJsonAntiForgeryTokenAttribute - OnAction Executing");


            HttpRequestBase request = filterContext.HttpContext.Request;
            HttpResponseBase response = filterContext.HttpContext.Response;

            // Capture the token name
            var tokenName = AntiForgeryConfig.CookieName;

            string cookieToken = request.Cookies[tokenName] != null
                ? request.Cookies[tokenName].Value
                : null;

            var postData = HttpUtils.JsonInputStreamAsDictionary(request.InputStream);
            string formToken = (postData != null && postData.ContainsKey(tokenName))
                ? postData[tokenName]
                : null;

            try
            {
                AntiForgery.Validate(cookieToken, formToken);
                Serilog.Log.Logger.Information(string.Format("ValidateJsonAntiForgeryTokenAttribute -  AntiForgery token is valid - {0}", postData != null ? JsonConvert.SerializeObject(postData) : ""));
            }
            catch (HttpAntiForgeryException e)
            {
                Serilog.Log.Logger.Error(string.Format("ValidateJsonAntiForgeryTokenAttribute - AntiForgery token is invalid - {0}", postData != null ? JsonConvert.SerializeObject(postData) : ""));
                filterContext.Result = CommonUtils.StandardizedJsonResponse(
                    "AntiForgery token is invilid.",
                    null,
                    HttpStatusCode.PreconditionFailed
                    );
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
