﻿using retry.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace retry.Services
{
    public class GithubService: IGithubService
    {
        private readonly HttpClient _httpClient;
        public GithubService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        // https: //api.github.com  -- 403 forbidden response
        //public async Task<string> GetItems()
        //{
        //    var response = await _httpClient.GetAsync("");
        //    response.EnsureSuccessStatusCode();

        //    using var responseStream = await response.Content.ReadAsStreamAsync();
        //    return Utilities.ToEncodedString(responseStream);
        //}

        public async Task<string> GetItems()
        {
            // https: //api.github.com  -- 403 forbidden response

            var response = await _httpClient.GetAsync("");
            using var responseStream = await response.Content.ReadAsStreamAsync();
            return Utilities.ToEncodedString(responseStream);
        }
    }
}
