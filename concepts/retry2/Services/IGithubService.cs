﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace retry.Services
{
    public interface IGithubService
    {
        Task<string> GetItems();
    }
}
