﻿using Microsoft.AspNetCore.Mvc;
using retry.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace retry.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RetryController : Controller
    {
        private IGithubService _githubService;

        public RetryController(IGithubService githubService) => _githubService = githubService;
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("getRetry")]
        public async Task<string> GetRetry()
        {
            var response = await _githubService.GetItems();
            return response;
        }
    }
}
