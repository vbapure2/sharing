using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using retry.DelegateHandlers;
using retry.Services;

namespace retry
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddTransient<SecureRequestHandler>();
            services.AddTransient<ValidateHeaderHandler>();

            //Set 5 min as the lifetime for the HttpMessageHandler objects in the pool used for the Catalog Typed Client
            services.AddHttpClient<IGithubService, GithubService>(client =>
            {
                client.BaseAddress = new Uri("https://api.thecatapi.com/v1/images/search");
                client.DefaultRequestHeaders.Add("Accept", "application/vnd.github.v3+json");
                client.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            })
            .SetHandlerLifetime(TimeSpan.FromMinutes(5))
            .AddHttpMessageHandler<SecureRequestHandler>()
            .AddHttpMessageHandler<ValidateHeaderHandler>()
            .AddPolicyHandler(GetRetryPolicy());
            //.AddTransientHttpErrorPolicy(p => p.WaitAndRetryAsync(3, (retryAttempt) => {
            //    Console.WriteLine($"Attempt {retryAttempt}. Waiting 10 seconds");
            //    return TimeSpan.FromMilliseconds(10);
            // }));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        //static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        //{
        //    return HttpPolicyExtensions
        //        .HandleTransientHttpError()
        //        .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
        //        .WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
        //}

        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            var retryPolicy = Policy
                            .Handle<HttpRequestException>((exception) => {
                                Console.WriteLine("Exception --- {0}", exception);
                                return true;
                            })
                            .OrResult<HttpResponseMessage>(response => {
                                Console.WriteLine("response --- {0}", response);
                                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest ||
                                    response.StatusCode == System.Net.HttpStatusCode.NotFound ||
                                    response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    return true;
                                }
                                return false;
                            })
                            .WaitAndRetryAsync(3, (retryAttempt) => {
                                Console.WriteLine("response --- {0}", retryAttempt);
                                return TimeSpan.FromSeconds(Math.Pow(2, retryAttempt));
                             });
                            //.WaitAndRetryAsync(new[]
                            //{
                            //    TimeSpan.FromSeconds(1),
                            //    TimeSpan.FromSeconds(5),
                            //    TimeSpan.FromSeconds(10)
                            //});

            return retryPolicy;
        }
    }
}
