﻿using integrationTesting.Models;
using integrationTesting.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace integrationTesting.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DogController : ControllerBase
    {

        private readonly ILogger<DogController> _logger;
        private readonly IDogService _dogService;

        public DogController(ILogger<DogController> logger, IDogService dogService)
        {
            _logger = logger;
            _dogService = dogService;
        }


        [HttpGet("GetBreed")]
        public async Task<IActionResult> GetDogs()
        {
            try
            {
                var response = await _dogService.GetDogBreed();
                var responseString = await response.Content.ReadAsStringAsync();

                var breed = JsonSerializer.Deserialize<DogBreed>(responseString);

                return Ok(breed);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "GetAn error occurred " + "Message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace);
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
