﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace integrationTesting.Models
{
    public class WeatherForcastModel
    {
        public string date { get; set; }
        public decimal temperatureC { get; set; }
        public decimal temperatureF { get; set; }
        public string summary { get; set; }
    }
}
