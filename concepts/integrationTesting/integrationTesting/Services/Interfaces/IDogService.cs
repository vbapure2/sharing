﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace integrationTesting.Services.Interfaces
{
    public interface IDogService
    {
        Task<HttpResponseMessage> GetDogBreed();
    }
}
