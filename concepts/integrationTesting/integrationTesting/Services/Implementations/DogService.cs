﻿using integrationTesting.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace integrationTesting.Services.Implementations
{
    public class DogService: IDogService
    {

        public async Task<HttpResponseMessage> GetDogBreed()
        {
            var client = new HttpClient();
            return await client.GetAsync("https://api.thedogapi.com/v1/breeds/2");
        }
    }
}
