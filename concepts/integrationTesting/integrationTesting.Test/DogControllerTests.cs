﻿using integrationTesting.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace integrationTesting.Test
{
    public class DogControllerTests
    {
        private CustomWebApplicationFactory<integrationTesting.Startup> _factory;

        [OneTimeSetUp]
        public void SetUp()
        {
            _factory = new CustomWebApplicationFactory<Startup>();
        }

        [TestCase("/dog/GetBreed", "African Hunting Dog1")]
        //[TestCase("/dog/GetBreed", "No such host is known.")]
        public async Task Get_DogBreed(string url, string expectedBreedName)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299

            var responseString = await response.Content.ReadAsStringAsync();

            var actualBreed = JsonSerializer.Deserialize<DogBreed>(responseString);
            Assert.AreEqual(expectedBreedName, actualBreed.name);

        }
    }
}
