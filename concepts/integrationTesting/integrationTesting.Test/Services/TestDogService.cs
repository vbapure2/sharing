﻿using integrationTesting.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace integrationTesting.Test.Services
{
    public class TestDogService: IDogService
    {
        public async Task<HttpResponseMessage> GetDogBreed()
        {
            var client = new HttpClient();
            return await client.GetAsync("https://api.thedogapi.com/v1/breeds/3");
        }
    }
}
