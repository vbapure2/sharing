﻿using integrationTesting.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace integrationTesting.Test
{
    [TestFixture]
    public class WeatherForecastControllerTests
    {
        private CustomWebApplicationFactory<integrationTesting.Startup> _factory;

        [OneTimeSetUp]
        public void SetUp()
        {
            _factory = new CustomWebApplicationFactory<Startup>();
        }

        [TestCase("/weatherforecast")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {
            // Arrange
            var client = _factory.CreateClient();

            // Act
            var response = await client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299

            var responseString = await response.Content.ReadAsStringAsync();

            var forecastModels = JsonSerializer.Deserialize<WeatherForcastModel[]>(responseString);

            var res = DateTime.Now.CompareTo(DateTime.Parse(forecastModels[0].date).Date);
            Assert.AreEqual(DateTime.Now.ToString("dd-MM-yyyy"), DateTime.Parse(forecastModels[0].date).Date.ToString("dd-MM-yyyy"));
         
        }
    }
}
