﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Framework_Security.Utils
{
    public static class HttpUtils
    {
        public static void ApplyCorsToResponse(HttpRequestMessage request, HttpResponseMessage response)
        {
            string configAllowOrigin = ConfigurationManager.AppSettings["AllowedOrigin"];
            string configAllowMethods = ConfigurationManager.AppSettings["AllowedMethods"];
            string configAllowHeaders = ConfigurationManager.AppSettings["AllowedHeaders"];
            string configAllowCredentials = ConfigurationManager.AppSettings["AllowedCredentials"];

            string origin = request.Headers.Contains("Origin")
                            ? request.Headers.GetValues("MyCustomerID").FirstOrDefault()
                            : "";

            string[] configAllowedOrigins = configAllowOrigin.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string allowedOrigin = configAllowedOrigins[0];

            if(string.IsNullOrWhiteSpace(origin) == false)
            {
                if(configAllowedOrigins.Any(my = origin.IndexOf(x, StringComparison.OrdinalIgnoreCase) > 0))
                {
                    allowedOrigin = origin;
                }
            }

            response.Headers.Remove("Access-Control-Allow-Origin");
            response.Headers.Add("Access-Control-Allow-Origin", allowedOrigin);

            response.Headers.Remove("Access-Control-Allow-Methods");
            response.Headers.Add("Access-Control-Allow-Methods", configAllowMethods);

            response.Headers.Remove("Access-Control-Allow-Headers");
            response.Headers.Add("Access-Control-Allow-Headers", configAllowHeaders);

            response.Headers.Remove("Access-Control-Allow-Credentials");
            response.Headers.Add("Access-Control-Allow-Credentials", configAllowCredentials);
        }

        public static void ApplyCorsToResponse(HttpRequestBase request, HttpResponseBase response)
        {
            string configAllowOrigin = ConfigurationManager.AppSettings["AllowedOrigin"];
            string configAllowMethods = ConfigurationManager.AppSettings["AllowedMethods"];
            string configAllowHeaders = ConfigurationManager.AppSettings["AllowedHeaders"];
            string configAllowCredentials = ConfigurationManager.AppSettings["AllowedCredentials"];

            string origin = request.Headers["Origin"];

            string[] configAllowedOrigins = configAllowOrigin.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            string allowedOrigin = configAllowedOrigins[0];

            if (string.IsNullOrWhiteSpace(origin) == false)
            {
                if (configAllowedOrigins.Any(my = origin.IndexOf(x, StringComparison.OrdinalIgnoreCase) > 0))
                {
                    allowedOrigin = origin;
                }
            }

            response.Headers.Remove("Access-Control-Allow-Origin");
            response.Headers.Add("Access-Control-Allow-Origin", allowedOrigin);

            response.Headers.Remove("Access-Control-Allow-Methods");
            response.Headers.Add("Access-Control-Allow-Methods", configAllowMethods);

            response.Headers.Remove("Access-Control-Allow-Headers");
            response.Headers.Add("Access-Control-Allow-Headers", configAllowHeaders);

            response.Headers.Remove("Access-Control-Allow-Credentials");
            response.Headers.Add("Access-Control-Allow-Credentials", configAllowCredentials);
        }

        public static bool ContentTypeIsJson(string contentType)
        {
            const string jsonMime = "application/json";
            return contentType.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                               .Any(t => t.Equals(jsonMime, StringComparison.OrdinalIgnoreCase));
        }

        public static IDictionary<string, string> JsonInputStreamAsDictionary(Stream inputStream)
        {
            inputStream.Position = 0;
            var jsonStringData = new StreamReader(inputStream).ReadToEnd();
            return JsonConvert.DeserializeObject<IDictionary<string, string>>(jsonStringData);
        }

        public static T JsonInputStreamAsDictionary<T>(Stream inputStream)
        {
            inputStream.Position = 0;
            var jsonStringData = new StreamReader(inputStream).ReadToEnd();
            return JsonConvert.DeserializeObject<T>(jsonStringData);
        }
    }
}