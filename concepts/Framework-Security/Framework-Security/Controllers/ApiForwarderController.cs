﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Framework_Security.Controllers
{
    public class ApiForwarderController
    {

        [HttpPost]
        [Route("lending/api/initialise")]
        public async Task<HttpResponseMessage> InitialiseSecureContent()
        {
            try
            {
                var myRequest = ((HttpContextWrapper)Request.Properties["MS_HttpContext"]).Request;
                var ipAddress = string.IsNullOrEmpty(myRequest.ServerVariables["HTTP_X_FORWARDER_FOR"]) ? myRequest.UserHostAddress : myRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];
                var userAgent = Request.Headers.UserAgent?.FirstOrDefault()?.Product;
                var identifier = await Request.Content.ReadAsStringAsync();
                var id = ApiHeler.SetCookie(identifier);
                var response = ApiHelper.SetKey(id);
                return Request.CreateResponse(HttpStatusCode.OK, EncodeBased64String(response));

            }
        }

        private string SetCookie(string id)
        {
            id = id ?? new Guid().ToString();
            id = Convert.ToBase64String(Encoding.ASCII.GetBytes(id));
            WriteCookie(id);
            return id;
        }

        private string SetKey(string key)
        {
            var rsaProider = new RSACryptoServiceProvider();
            var cacheStore = new RedisCacheStore();
            cacheStore.AddOrSet(key, Convert.ToBase64String(Encoding.UTF8.GetBytes(rsaProider.ToXmlString(true)), new TimeSpan(0, _keyExpiry, 0));
            return SecurityUtils.ExportPublicKey(rsaProvider);
        }

        private string WriteCookie(string id)
        {
            HttpCookie cookie = new HttpCookie("enK", id)
            {
                HttpOnly = true,
                Secure = true,
                Expires = DateTime.Now.AddMinutes(_keyExpiry)
            };
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}