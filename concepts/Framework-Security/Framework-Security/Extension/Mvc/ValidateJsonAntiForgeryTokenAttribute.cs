﻿using Framework_Security.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Framework_Security.Extension.Mvc
{
    public class ValidateJsonAntiForgeryTokenAttribute: ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase request = filterContext.HttpContext.Request;
            HttpResponseBase response = filterContext.HttpContext.Response;

            // Capture the token name
            // Ref: namespace System.Web.Helpers.AntiForgeryConfig.AntiForgeryTokenFieldName = "__RequestVerificationToken"
            var fieldName = "__RequestVerificationToken";
            var cookieName = AntiForgeryConfig.CookieName;

            string cookieToken = request.Cookies[cookieName] != null
                                ? request.Cookies[cookieName].Value
                                : null;

            var postData = HttpUtils.JsonInputStreamAsDictionary(request.InputStream);
            string formToken = (postData != null && postData.ContainsKey(fieldName))
                                ? postData[fieldName]
                                : null;

            try
            {
                AntiForgery.Validate(cookieToken, formToken);
            }
            catch(HttpAntiForgeryException e)
            {
                // HttpStatusCode.PreconditionFailed
                filterContext.Result = new HttpResponse("AntiForgery token is invalid");
            }

            base.OnActionExecuting(filterContext);
        }
    }
}