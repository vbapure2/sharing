﻿using Framework_Security.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Framework_Security.Extension.Mvc
{
    public class ValidateRecaptchaAttribute : ActionFilterAttribute, IActionFilter
    {
        private const string RECAPTCHA_FORM_FEILD_NAME = "g-recaptcha-response";
        private const string RECAPTCHA_FAILED_MESSAGE = "Please confirm you are not a robot.";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string secretKey = ConfigurationManager.AppSettings["Recaptcha Key"] ?? "";

            bool recaptchaPassed = true;
            if(string.IsNullOrWhiteSpace(secretKey) == false)
            {
                recaptchaPassed = false;

                string gRecapchaResponse = null;
                bool isJsonRequest = HttpUtils.ContentTypeIsJson(filterContext.HttpContext.Request.ContentType);
                if(isJsonRequest == true)
                {
                    var postData = HttpUtils.JsonInputStreamAsDictionary(filterContext.HttpContext.Request.InputStream);
                    gRecapchaResponse = postData.ContainsKey(RECAPTCHA_FORM_FEILD_NAME)
                                    ? postData[RECAPTCHA_FORM_FEILD_NAME]
                                    : null;
                }
                else
                {
                    gRecapchaResponse = filterContext.HttpContext.Request[RECAPTCHA_FORM_FEILD_NAME] ?? null; 
                }

                if(string.IsNullOrWhiteSpace(gRecapchaResponse) == false)
                {
                    using(WebClient client = new WebClient())
                    {
                        var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, gRecapchaResponse));
                        var obj = JObject.Parse(result);
                        recaptchaPassed = (bool)obj.SelectToken("success");
                    }
                }

                if(recaptchaPassed == false)
                {
                    filterContext.Controller.ViewData.ModelState.AddModelError(string.Empty, RECAPTCHA_FAILED_MESSAGE);
                    filterContext.Controller.TempData["RecaptchaFailed"] = RECAPTCHA_FAILED_MESSAGE;
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}