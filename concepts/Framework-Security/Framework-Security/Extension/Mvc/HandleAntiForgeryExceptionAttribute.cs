﻿using Framework_Security.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Framework_Security.Extension.Mvc
{
    public class HandleAntiForgeryExceptionAttribute: HandleErrorAttribute
    {
        private const string CSRF_EXCEPTION_MESSAGE = "Anti-forgery token is invalid. Please refresh the page and try again.";

        public HandleAntiForgeryExceptionAttribute()
        {
            this.ExceptionType = typeof(HttpAntiForgeryException);
        }

        public override void OnException(ExceptionContext filterContext)
        {
            if(this.ExceptionType.IsAssignableFrom(filterContext.Exception.GetType()))
            {
                filterContext.ExceptionHandled = true;
                filterContext.Controller.ViewData.ModelState.AddModelError(string.Empty, CSRF_EXCEPTION_MESSAGE);

                filterContext.Controller.TempData["AntiForgeryException"] = CSRF_EXCEPTION_MESSAGE;

                HttpUtils.ApplyCorsToResponse(filterContext.HttpContext.Request, filterContext.HttpContext.Response);

                filterContext.Result = new RedirectResult(filterContext.HttpContext.Request.RawUrl);
            }
        }
    }
}