﻿using Framework_Security.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Framework_Security.Extension.Mvc
{
    public class JsonAllowCrossSiteAttribute: ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase request = filterContext.HttpContext.Request;
            HttpResponseBase response = filterContext.HttpContext.Response;

            HttpUtils.ApplyCorsToResponse(request, response);

            // Check for Pre-flight OPTIONS request
            if(request.Headers.AllKeys.Contains("Origin", StringComparer.OrdinalIgnoreCase) && request.HttpMethod == "OPTIONS")
            {
                filterContext.Result = new EmptyResult();
                response.StatusCode = (int)HttpStatusCode.OK;
                response.End();
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}