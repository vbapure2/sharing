﻿using Framework_Security.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Framework_Security.Extension.WebApi
{
    public class JsonAllowCrossSiteAttribute: ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionContext)
        {
            HttpRequestMessage request = actionContext.Request;
            HttpResponseMessage response = actionContext.Response;

            HttpUtils.ApplyCorsToResponse(request, response);
            base.OnActionExecuted(actionContext);
        }
    }
}