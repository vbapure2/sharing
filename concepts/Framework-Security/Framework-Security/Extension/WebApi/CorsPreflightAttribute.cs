﻿using Framework_Security.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Framework_Security.Extension.WebApi
{
    public class CorsPreflightAttribute: ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            // Check for Pre-flight OPTIONS request
            HttpRequestMessage request = actionContext.Request;

            if(request.Headers.Contains("Origin") && request.Method == HttpMethod.Options)
            {
                HttpResponseMessage response = new HttpResponseMessage();
                HttpUtils.ApplyCorsToResponse(request, response);
                response.StatusCode = System.Net.HttpStatusCode.OK;
                actionContext.Response = response;
                return;
            }
            base.OnActionExecuting(actionContext);
        }
    }
}