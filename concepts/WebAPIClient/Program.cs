﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace WebAPIClient
{
    class Program
    {
        private static readonly HttpClient client = new HttpClient();

        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            PrintThreadInfo("Main Thread Before Async Task Call");

            // ---------------------------------------------------------------------------

            //var cancellationTokenSource = new CancellationTokenSource();

            //var task1 = TestAsynMethod1(cancellationTokenSource.Token);

            //await Task.Delay(100);
            //Console.WriteLine("Main Thread Sleep Complete");
            //cancellationTokenSource.Cancel();

            //await task1;

            //Console.WriteLine("task1 IsCompleted -- {0}, IsFaulted -- {0}, IsCancelled -- {0}", 
            //                    task1.IsCompleted, task1.IsFaulted, task1.IsCanceled);

            //await Task.WhenAll(task1);

            // ---------------------------------------------------------------------------
            //TestAsynMethod2();
            //await ProcessRepositories();
            //await TestAsynMethod3();

            // ---------------------------------------------------------------------------
            //    Cancellation fault test
            // ---------------------------------------------------------------------------

            //try
            //{
            //    var cancellationTask = TestAsynMethod_CancellationFault(cancellationTokenSource.Token);
            //    await cancellationTask;
            //}
            //catch(Exception ex)
            //{
            //    PrintThreadInfo("TestAsynMethod_CancellationFault Exception Handling");
            //    Console.WriteLine("Task Exception Message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace);
            //}

            //---------------------------------------------------------------------------
            //   InProgress test
            // ---------------------------------------------------------------------------

            try
            {
                var progress = new Progress<int>((progressReport) => { 
                    Console.WriteLine("Progress Report -- {0}", progressReport);
                    PrintThreadInfo("Thread that received Progress Report");
                });

                var progressTask = TestAsynMethod_InProgress(progress);
                await progressTask;
            }
            catch (Exception ex)
            {
                PrintThreadInfo("TestAsynMethod_InProgress Exception Handling");
                Console.WriteLine("Task Exception Message: {0}, StackTrace: {1}", ex.Message, ex.StackTrace);
            }


            // ---------------------------------------------------------------------------
            // ---------------------------------------------------------------------------

            // ---------------------------------------------------------------------------
            // ---------------------------------------------------------------------------


            // ---------------------------------------------------------------------------
            // ---------------------------------------------------------------------------


            PrintThreadInfo("Main Thread After Async Task Call");
            
        }

        private static async Task TestAsynMethod1(CancellationToken cancellationToken)
        {
            await Task.Factory.StartNew(async () => {
                PrintThreadInfo("TestAsynMethod1 Task");

                while(!cancellationToken.IsCancellationRequested)
                {
                    PrintThreadInfo("TestAsynMethod1 Task");
                    Console.WriteLine("Please enter any key");
                    Console.ReadLine();
                }

                if(cancellationToken.IsCancellationRequested)
                {
                    Console.WriteLine("Cancellation Requested: TestAsynMethod1 Task");
                }
                await Task.Delay(500);
                Console.WriteLine("TestAsynMethod1 Task Completing");
            }, cancellationToken);
        }

        private static async Task TestAsynMethod_CancellationFault(CancellationToken cancellationToken)
        {
            await Task.Factory.StartNew(() => {
                PrintThreadInfo("TestAsynMethod CancellationFault");

                throw new Exception("Test exception");
                
            }, cancellationToken);
        }

        private static async Task TestAsynMethod_InProgress(IProgress<int> progress)
        {
            await Task.Factory.StartNew(async () => {
                PrintThreadInfo("TestAsynMethod InProgress");

                for (int i = 0; i != 10; ++i)
                {
                    Thread.Sleep(100); // CPU-bound work
                    if (progress != null)
                    {
                        Console.WriteLine("\n");
                        PrintThreadInfo("Tread Reporting Progress");
                        progress.Report(i);
                    }
                        
                }
            });
        }

        private static async Task TestAsynMethod2()
        {
            await Task.Factory.StartNew(async () => {
                PrintThreadInfo("TestAsynMethod2 Task");
                await Task.Delay(400);
                Console.WriteLine("TestAsynMethod2 Task Completing");
            });
        }

        private static async Task TestAsynMethod3()
        {
            await Task.Factory.StartNew(() => {
                PrintThreadInfo("TestAsynMethod3 Task");
                Console.WriteLine("TestAsynMethod3 Task Completing");
            });
        }

        private static async Task ProcessRepositories()
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

            PrintThreadInfo("ProcessRepositories Task");

            var stringTask = client.GetStringAsync("https://api.github.com/orgs/dotnet/repos");

            var msg = await stringTask;
            // Console.Write(msg);
        }

        static void PrintThreadInfo(string taskName)
        {
            var thread = System.Threading.Thread.CurrentThread;
            Console.WriteLine("Async Task: Task Name: {0}, " +
                                "Thread Name -- {1}, " +
                                "ThreadId -- {2}, " +
                                "IsThreadPoolThread -- {3}, " +
                                "IsBackgroundThread -- {4} " +
                                "IsAlive -- {5}"
                                , taskName
                                , thread.Name
                                , thread.ManagedThreadId
                                , thread.IsThreadPoolThread
                                , thread.IsBackground
                                , thread.IsAlive);
        }


    }
}
