﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace retry.DataAccess
{
    public class OrdersDBContext : DbContext
    {
        public OrdersDBContext(DbContextOptions<OrdersDBContext> options)
        : base(options) { }

        public DbSet<Models.Order> Orders { get; set; }

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                //3. Get the instance of BoardGamesDBContext in our services layer
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<OrdersDBContext>();

                if (context.Orders.Any())
                {
                    return;   // Data was already seeded
                }

                context.Orders.AddRange(
                    new Models.Order
                    {
                        Id = 1,
                        Item = "Carpet",
                        Quantity = 3
                    },
                    new Models.Order
                    {
                        Id = 2,
                        Item = "Shoes",
                        Quantity = 2
                    }
               );

                context.SaveChanges();
            }
        }
    }

   
}
