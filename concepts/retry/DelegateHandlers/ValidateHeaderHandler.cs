﻿using retry.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace retry.DelegateHandlers
{
    public class ValidateHeaderHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request,
            CancellationToken cancellationToken)
        {
            return new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent("Response from ValidateHeaderHandler Delegate Handler")
            };

            if (!request.Headers.Contains("X-API-KEY"))
            {
                // throw new HttpRequestException();

                //return new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("You must supply an API key header called X-API-KEY")
                //};

                //return new HttpResponseMessage(HttpStatusCode.RequestTimeout)
                //{
                //    Content = new StringContent("You must supply an API key header called X-API-KEY")
                //};

            }

            return await base.SendAsync(request, cancellationToken);
        }
    }
}
