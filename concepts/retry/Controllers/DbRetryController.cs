﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using retry.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace retry.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DbRetryController: Controller
    {
        private readonly OrdersDBContext _context;

        public DbRetryController(OrdersDBContext context) => _context = context;

        [HttpGet("getDbRetry")]
        public async Task<IActionResult> Get()
        {
            var orders = await _context.Orders
                .ToArrayAsync();

            var response = orders.Select(u => new
            {
                Id = u.Id,
                Item = u.Item,
                Quantity = u.Quantity
            });

            return Ok(response);
        }

    }
}
