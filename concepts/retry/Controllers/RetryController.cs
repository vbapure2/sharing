﻿using Microsoft.AspNetCore.Mvc;
using Polly.CircuitBreaker;
using retry.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace retry.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RetryController : Controller
    {
        private IGithubService _githubService;

        public RetryController(IGithubService githubService) => _githubService = githubService;
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("getRetry")]
        public async Task<IActionResult> GetRetry()
        {
            try
            {
                var response = await _githubService.GetItems();
                return Ok(response);
            }
            catch(BrokenCircuitException brex)
            {
                HandleBrokenCircuitException();
                return StatusCode(500, "Basket Service is inoperative, please try later on. (Business message due to Circuit-Breaker)");
                //return Ok("Basket Service is inoperative, please try later on. (Business message due to Circuit-Breaker)");
            }
        }

        private void HandleBrokenCircuitException()
        {

        }
    }
}
