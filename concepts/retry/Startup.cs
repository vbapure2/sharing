using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using retry.DataAccess;
using retry.DelegateHandlers;
using retry.Services;

namespace retry
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Default Execution Strategy, Custom Execution Strategy - Examples
            // https: //www.thecodebuzz.com/database-connection-resiliency-entity-framework-asp-net-core/
            services.AddDbContext<OrdersDBContext>(options => options.UseInMemoryDatabase(databaseName: "Orders") );
            //services.AddDbContext<OrdersDBContext>(options =>
            //{
            //    options.UseSqlServer(Configuration["ConnectionString"],
            //                        sqlServerOptionsAction: sqlOptions =>
            //                        {
            //                            sqlOptions.EnableRetryOnFailure(
            //                            maxRetryCount: 5,
            //                            maxRetryDelay: TimeSpan.FromSeconds(30),
            //                            errorNumbersToAdd: null);
            //                        });
            //});


            services.AddTransient<SecureRequestHandler>();
            services.AddTransient<ValidateHeaderHandler>();


            // Binding HttpClient to GithubService: It's also possible to bind the HttpClient to the service by supplying a generic parameter to AddHttpClient:

            //Set 5 min as the lifetime for the HttpMessageHandler objects in the pool used for the Catalog Typed Client
            services.AddHttpClient<IGithubService, GithubService>(client =>
            {
                // Hierarchical retry scenario -- should be careful -- open retry2 solution to simulate
                //client.BaseAddress = new Uri("http://localhost:18086/retry/getRetry");

                // Simulating Retry, Circuitbreaker
                //client.BaseAddress = new Uri("https://api.thecatapi.com/v1/images/search123");

                client.BaseAddress = new Uri("https://api.thecatapi.com/v1/images/search");
                client.DefaultRequestHeaders.Add("Accept", "application/vnd.github.v3+json");
                client.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            })
            .SetHandlerLifetime(TimeSpan.FromMinutes(5))
            .AddPolicyHandler(GetRetryPolicy())
            .AddPolicyHandler(GetCircuitBreakerPolicy())
            .AddHttpMessageHandler<SecureRequestHandler>()
            .AddHttpMessageHandler<ValidateHeaderHandler>();


            // If you want to mock a resposne from the delegate handler and apply polly retry policy use below order
            //services.AddHttpClient<IGithubService, GithubService>(client => { })
            //        .AddPolicyHandler(GetRetryPolicy())
            //        .AddHttpMessageHandler<ValidateHeaderHandler>();

            // Below order of delegate handler and apply polly retry policy will not help to mock the response from
            // delegate handler
            //services.AddHttpClient<IGithubService, GithubService>(client => { })
            //        .AddHttpMessageHandler<ValidateHeaderHandler>()
            //        .AddPolicyHandler(GetRetryPolicy());

            // Unit test: For mocking stub delegate handler refer to
            // https: //stackoverflow.com/questions/61254623/unit-test-httpclient-with-polly


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            OrdersDBContext.Initialize(app.ApplicationServices);

            //using (var scope = app.ApplicationServices.CreateScope())
            //{
            //    //3. Get the instance of BoardGamesDBContext in our services layer
            //    var services = scope.ServiceProvider;
            //    var context = services.GetRequiredService<OrdersDBContext>();

            //    //4. Call the DataGenerator to create sample data
            //    OrdersDBContext.Initialize(context);
            //}


        }

        //static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        //{
        //    return HttpPolicyExtensions
        //        .HandleTransientHttpError()
        //        .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
        //        .WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
        //}

        static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            var retryPolicy = Policy
                            .Handle<HttpRequestException>((exception) => {
                                Console.WriteLine("Exception --- {0}", exception);
                                return true;
                            })
                            .OrResult<HttpResponseMessage>(response => {
                                Console.WriteLine("response --- {0}", response);
                                
                                // To test hierarchical retry
                                //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                //{
                                //    return true;
                                //}

                                if (response.StatusCode == System.Net.HttpStatusCode.BadRequest ||
                                    response.StatusCode == System.Net.HttpStatusCode.NotFound)
                                {
                                    return true;
                                }
                                return false;
                            })
                            .WaitAndRetryAsync(3, (retryAttempt) => {
                                Console.WriteLine("response --- {0}", retryAttempt);
                                return TimeSpan.FromSeconds(Math.Pow(2, retryAttempt));
                             });
                            //.WaitAndRetryAsync(new[]
                            //{
                            //    TimeSpan.FromSeconds(1),
                            //    TimeSpan.FromSeconds(5),
                            //    TimeSpan.FromSeconds(10)
                            //});

            return retryPolicy;
        }

        static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return Policy
                    .Handle<HttpRequestException>((exception) =>
                    {
                         Console.WriteLine("Exception --- {0}", exception);
                         return true;
                    })
                    .OrResult<HttpResponseMessage>(response => {
                        Console.WriteLine("response --- {0}", response);

                        if (response.StatusCode == System.Net.HttpStatusCode.BadRequest ||
                               response.StatusCode == System.Net.HttpStatusCode.NotFound)
                        {
                            return true;
                        }
                        
                        return false;
                    })
                    .CircuitBreakerAsync(2, TimeSpan.FromSeconds(30));
        }
    }
}
