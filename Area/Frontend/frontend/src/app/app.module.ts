import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AreaListComponent } from './area-list/area-list.component';
import { AreaEditComponent } from './area-edit/area-edit.component';
import { AreaService } from './shared/services/area.service';

@NgModule({
  declarations: [
    AppComponent,
    AreaListComponent,
    AreaEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule

  ],
  providers: [AreaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
