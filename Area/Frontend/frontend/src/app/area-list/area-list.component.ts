import { Component, OnInit} from '@angular/core';
import {AreaService} from '../shared/services/area.service';
import {Area} from '../shared/model/area';
import {map, finalize} from 'rxjs/operators';

@Component({
  selector: 'app-area-list',
  templateUrl: './area-list.component.html',
  styleUrls: ['./area-list.component.scss']
})
export class AreaListComponent implements OnInit {

  loading: boolean = true;
  data: Area[] = [];
  isError: boolean = false;
  errorMsg: string;
  infoMsg: string;

  constructor(private areaService: AreaService) {}

  ngOnInit() {
    this.areaService
        .getAreas('/api/Areas')
        .pipe(finalize(() => this.loading = false))
        .subscribe(
          data => {
            this.data = data;
          },
          fail => {
            this.isError = true;
            this.errorMsg = fail.error == null ? 'Failed to load Areas' : fail.error.message;
          });
  }

  onDelete(area: Area) {
    this.areaService.deleteArea('/api/Areas/' + area.id)
        .subscribe(
            success => {
              this.infoMsg = 'Successfully deleted ' + area.name;
              this.data = this.data.filter(_ => _.id != area.id);
            },
            fail => {
              this.isError = true;
              this.errorMsg = fail.error == null ? 'Failed to Delete Area' : fail.error.message;
            }
        );
  }

}
