import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreaListComponent } from './area-list/area-list.component';
import { AreaEditComponent } from './area-edit/area-edit.component';

const routes: Routes = [
  {path: '', redirectTo: 'area-list', pathMatch: 'full'},
  {path: 'area-list', component: AreaListComponent},
  {path: 'area-edit/:id', component: AreaEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
