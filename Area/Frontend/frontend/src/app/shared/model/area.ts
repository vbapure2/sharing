export interface Area {
    id: number;
    name: string;
    createdOn: Date;
    createdBy: number;
    updatedOn?: Date;
    updatedBy?: number;
}
