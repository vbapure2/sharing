import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, pipe, of } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import { Area } from '../model/area';


@Injectable({
  providedIn: 'root'
})
export class AreaService {

  params: HttpParams;

  constructor(private http: HttpClient) { 
    this.params = new HttpParams().set('Content-Type', 'application/json');
  }

  getAreas(api: string) : Observable<any> {  
    return this.http.get(api);
  }

  updateArea(api: string, area: Area) : Observable<any> {  
    return this.http.put(api, area);
  }

  deleteArea(api: string) : Observable<any> {  
    return this.http.delete(api);
  }
  
}
