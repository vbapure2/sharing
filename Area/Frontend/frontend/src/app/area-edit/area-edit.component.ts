import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router'
import {map, finalize} from 'rxjs/operators';

import { Area } from '../shared/model/area';
import { AreaService } from '../shared/services/area.service';

@Component({
  selector: 'app-area-edit',
  templateUrl: './area-edit.component.html',
  styleUrls: ['./area-edit.component.scss']
})
export class AreaEditComponent implements OnInit {
  loading: boolean;
  updated: boolean;
  areaId: number;
  data: Area;
  isError: boolean;
  errorMsg: string;

  constructor(private router: Router, 
              private route: ActivatedRoute, 
              private areaService: AreaService) { 
    this.loading = true;
    this.updated = false;
    this.isError = false;
    this.areaId = +this.route.snapshot.params['id'];
  }
  
  ngOnInit() {
    this.areaService
        .getAreas('/api/Areas/' + this.areaId)
        .pipe(finalize(() => this.loading = false))
        .subscribe(
          data => {
            this.data = data;
            console.log("Area: ", this.data);
          },
          fail => {
            console.log("Area: ", this.data);
            if(!this.data) {
              console.log("Data is not defined ");
            }
            this.isError = true;
            this.errorMsg = fail.error == null ? 'Failed to load Area' : fail.error.message;
          });
  }

  onSave() {
    this.areaService.updateArea('/api/Areas/' + this.data.id, this.data)
        .subscribe(
            success => {
              this.updated = true;
              this.router.navigate(['../area-list']);
            },
            fail => {
              this.isError = true;
              this.errorMsg = fail.error == null ? 'Failed to Update Area' : fail.error.message;
            }
        );
  }

  onDelete() {
    this.areaService.deleteArea('/api/Areas/' + this.data.id)
        .subscribe(
            success => {
              this.updated = true;
              this.router.navigate(['../area-list']);
            },
            fail => {
              this.isError = true;
              this.errorMsg = fail.error == null ? 'Failed to Delete Area' : fail.error.message;
            }
        );
  }

}
