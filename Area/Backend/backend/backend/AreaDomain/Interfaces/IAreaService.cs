﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.AreaDomain.Interfaces
{
    public interface IAreaService
    {
        AreaViewModel GetArea(int id);
        Area GetArea(string name);
        List<AreaViewModel> GetAllAreas();
        void UpdateArea(Area area);
        void DeleteArea(int id);
    }
}
