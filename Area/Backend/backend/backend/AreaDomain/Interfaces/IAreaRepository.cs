﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.AreaDomain.Interfaces
{
    public interface IAreaRepository
    {
        Area GetArea(int id);
        Area GetArea(string name);
        List<Area> GetAllAreas();
        void AddArea(Area area);
        void UpdateArea(Area area);
        void DeleteAreas(int id);
    }
}
