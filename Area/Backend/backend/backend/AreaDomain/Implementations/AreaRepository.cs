﻿using backend.AreaDomain.Interfaces;
using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.AreaDomain.Implementations
{
    public class AreaRepository : IAreaRepository
    {
        private AreaContext _areaContext;

        public AreaRepository(AreaContext areaContext)
        {
            _areaContext = areaContext;
        }
        
        public Area GetArea(int id)
        {
            return _areaContext.Areas.Where(_ => _.Id == id).First();
        }

        public Area GetArea(string name)
        {
            var areas = _areaContext.Areas.Where(_ => _.Name == name);
            var res = areas.Count() == 0 ? null : areas.First();
            return res;
        }
        public List<Area> GetAllAreas()
        {
            return _areaContext.Areas.ToList();
        }

        public void AddArea(Area area)
        {
            _areaContext.Add(area);
            _areaContext.SaveChanges();
        }

        public void UpdateArea(Area area)
        {
            var dbarea = _areaContext.Areas.Where(_ => _.Id == area.Id).First();
            if (dbarea != null)
            {
                dbarea.Name = area.Name;
                dbarea.UpdatedOn = DateTime.UtcNow;
                _areaContext.SaveChanges();
            }
        }

        public void DeleteAreas(int id)
        {
            var area = _areaContext.Areas.First(_ => _.Id == id);
            _areaContext.Areas.Remove(area);
            _areaContext.SaveChanges();
            return;
        }
    }
}
