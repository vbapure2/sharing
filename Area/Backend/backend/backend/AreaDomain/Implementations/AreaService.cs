﻿using backend.AreaDomain.Interfaces;
using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.AreaDomain.Implementations
{
    public class AreaService : IAreaService
    {
        private readonly IAreaRepository _areaRepository;

        public AreaService(IAreaRepository areaRepository)
        {
            _areaRepository = areaRepository;
        }
        
        public AreaViewModel GetArea(int id)
        {
            var area = _areaRepository.GetArea(id);
            return AutoMapper.Mapper.Map<AreaViewModel>(area);
        }

        public Area GetArea(string name)
        {
            return _areaRepository.GetArea(name);
        }

        public List<AreaViewModel> GetAllAreas()
        {
            var areas = _areaRepository.GetAllAreas();
            return AutoMapper.Mapper.Map<List<Area>, List<AreaViewModel>>(areas);
        }
        public void UpdateArea(Area area)
        {
            _areaRepository.UpdateArea(area);
        }

        public void DeleteArea(int id)
        {
            _areaRepository.DeleteAreas(id);
        }
    }
}
