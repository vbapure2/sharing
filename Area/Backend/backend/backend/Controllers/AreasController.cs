﻿using backend.AreaDomain.Interfaces;
using backend.Filters;
using backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreasController : ControllerBase
    {
        private readonly IAreaService _areaService;
        public AreasController(IAreaService areaService)
        {
            _areaService = areaService;
        }

        // GET: api/Areas
        [HttpGet]
        public IActionResult GetAreas()
        {
            var areasvm = _areaService.GetAllAreas();
            if (!areasvm.Any())
            {
                return NotFound(new { message = "No Areas found in the database" });
            }

            return Ok(areasvm);
        }

        // GET: api/Areas/5
        [HttpGet("{id}")]
        public IActionResult GetArea([FromRoute] int id)
        {
            var areavm = _areaService.GetArea(id);
            if (areavm == null)
            {
                return NotFound(new { message = "This Area is not found in the database" });
            }

            return Ok(areavm);
        }

        // PUT: api/Areas/5
        [HttpPut("{id}")]
        [ValidateModel]
        public IActionResult PutArea([FromRoute] int id, [FromBody] Area area)
        {
            var repoarea = _areaService.GetArea(area.Name);
            if (repoarea != null && repoarea.Id != id)
            {
                return Conflict(new { message = $"An existing area with the name '{area.Name}' is already found." });
            }

            try
            {
                _areaService.UpdateArea(area);
            }
            catch (DbUpdateConcurrencyException)
            {
                var repoArea = _areaService.GetArea(id);
                if (repoArea != null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // DELETE: api/Areas/5
        [HttpDelete("{id}")]
        public IActionResult DeleteArea([FromRoute] int id)
        {
            var area = _areaService.GetArea(id);
            if(area == null)
            {
                return NotFound(new { message = "This Area is not found in the database" });
            }
            _areaService.DeleteArea(id);
            return NoContent();
        }
    }
}