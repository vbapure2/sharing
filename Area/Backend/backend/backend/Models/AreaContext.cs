﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    public class AreaContext : DbContext
    {
        public virtual DbSet<Area> Areas { get; set; }
        public AreaContext(DbContextOptions<AreaContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Area>().HasData(
                new Area {Id = 1, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 },
                new Area {Id = 2, Name = "Dining Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 },
                new Area {Id = 3, Name = "Kitchen Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 },
                new Area {Id = 4, Name = "Bed Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 },
                new Area {Id = 5, Name = "Hall Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 }
            );
        }

        
    
    }
}
