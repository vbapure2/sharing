﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace backend.Models
{
    public class Area
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [Column(TypeName = "nvarchar(200)")]
        public string Name { get; set; }

        [Required]
        [Column(TypeName = "datetimeoffset")]
        public DateTimeOffset CreatedOn { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int CreatedById { get; set; }

        [Column(TypeName = "datetimeoffset")]
        public DateTimeOffset? UpdatedOn { get; set; }

        [Column(TypeName = "int")]
        public int? UpdatedById { get; set; }
    }

    public static class CreatedByMapper
    {
        public static Dictionary<int, string> Map = new Dictionary<int, string> { { 1, "Admin" } };
    }
}
