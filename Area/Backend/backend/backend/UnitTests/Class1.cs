﻿using backend.AreaDomain.Implementations;
using backend.AreaDomain.Interfaces;
using backend.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using Xunit;

namespace UnitTests
{
    public class Class1
    {
        //[Fact]
        //public void PassingTest()
        //{
        //    Assert.Equal(4, Add(2, 2));
        //}

        //[Fact]
        //public void FailingTest()
        //{
        //    Assert.Equal(5, Add(2, 2));
        //}

        //int Add(int x, int y)
        //{
        //    return x + y;
        //}

        //[Theory]
        //[InlineData(3)]
        //[InlineData(5)]
        //[InlineData(6)]
        //public void MyFirstTheory(int value)
        //{
        //    Assert.True(IsOdd(value));
        //}

        //bool IsOdd(int value)
        //{
        //    return value % 2 == 1;
        //}

        [Fact]
        public void AreaTest()
        {
            // In-memory database only exists while the connection is open
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            try
            {

                IAreaRepository sut = GetInMemoryPersonRepository(connection);
                var area = new Area();
                area.Name = "xyz";
                area.CreatedOn = DateTime.Now;
                area.CreatedById = 10;
                area.UpdatedOn = DateTime.Now;
                area.UpdatedById = 10;

                sut.AddArea(area);

                var fetchArea = sut.GetAllAreas();

                var count = fetchArea.Count;

            }
            finally
            {
                connection.Close();
            }

            

        }

        private IAreaRepository GetInMemoryPersonRepository(SqliteConnection connection)
        {
            DbContextOptions<AreaContext> options;
            var builder = new DbContextOptionsBuilder<AreaContext>();
            // builder.UseInMemoryDatabase("");
            // builder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFProviders.InMemory;Trusted_Connection=True;ConnectRetryCount=0");

            //var options = new DbContextOptionsBuilder<AreaContext>()
            //        .UseSqlite(connection)
            //        .Options;

            builder.UseSqlServer(connection);

            options = builder.Options;
            AreaContext areaDataContext = new AreaContext(options);
            areaDataContext.Database.EnsureDeleted();
            areaDataContext.Database.EnsureCreated();
            return new AreaRepository(areaDataContext);
        }
    }
}
