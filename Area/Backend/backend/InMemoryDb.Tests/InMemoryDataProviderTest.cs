﻿using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

// Refer link
// http s://www.c-sharpcorner.com/article/unit-testing-with-inmemory-provider-and-sqlite-in-memory-database-in-efcore/

namespace InMemoryDb.Tests
{
    public class InMemoryDataProviderTest
    {
        [Fact]
        public void Task_Add_Without_Relation()
        {
            //Arrange    
            var factory = new ConnectionFactory();

            //Get the instance of BlogDBContext  
            var context = factory.CreateContextForInMemory();

            var area = new Area() {Id =6, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };

            //Act    
            //var data = context.Areas.Add(area);
            context.Set<Area>().Add(area);

            context.SaveChanges();

            //Assert    
            //Get the post count  
            var areaCount = context.Areas.Count();
            if (areaCount != 0)
            {
                Assert.Equal(6, areaCount);
            }

            //Get single post detail  
            var areaPost = context.Areas.FirstOrDefault();
            if (areaPost != null)
            {
                Assert.Equal("Living Room", areaPost.Name);
            }
        }

        [Fact]
        public void Task_Add_With_Relation()
        {
            //Arrange    
            var factory = new ConnectionFactory();

            //Get the instance of BlogDBContext  
            var context = factory.CreateContextForInMemory();

            var area = new Area() { Id = 7, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };

            //Act    
            var data = context.Areas.Add(area);
            context.SaveChanges();

            //Assert    
            //Get the post count  
            var areaCount = context.Areas.Count();
            if (areaCount != 0)
            {
                Assert.Equal(6, areaCount);
            }

            //Get single post detail  
            var singleArea = context.Areas.FirstOrDefault();
            if (singleArea != null)
            {
                Assert.Equal("Living Room", singleArea.Name);
            }
        }

        // [Fact]
        //public void Task_Add_Time_Test()
        //{
        //    //Arrange    
        //    var factory = new ConnectionFactory();

        //    //Get the instance of BlogDBContext  
        //    var context = factory.CreateContextForInMemory();

        //    //Act   
        //    for (int i = 1; i <= 1000; i++)
        //    {
        //        var area = new Area() { Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };
        //        context.Areas.Add(area);
        //    }

        //    context.SaveChanges();


        //    //Assert    
        //    //Get the post count  
        //    var areaCount = context.Areas.Count();
        //    if (areaCount != 0)
        //    {
        //        Assert.Equal(1000, areaCount);
        //    }

        //    //Get single post detail  
        //    var singleArea = context.Areas.Where(x => x.Id == 1).FirstOrDefault();
        //    if (singleArea != null)
        //    {
        //        Assert.Equal("Living Room", singleArea.Name);
        //    }
        //}
    }
}
