﻿using backend.Models;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace InMemoryDb.Tests
{
    public class ConnectionFactory : IDisposable
    {
        #region IDisposable Support  
        private bool disposedValue = false; // To detect redundant calls  

        public AreaContext CreateContextForInMemory()
        {
            var option = new DbContextOptionsBuilder<AreaContext>().UseInMemoryDatabase(databaseName: "Test_Database").Options;

            var context = new AreaContext(option);
            if (context != null)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }

            return context;
        }

        public AreaContext CreateContextForSQLite()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var option = new DbContextOptionsBuilder<AreaContext>().UseSqlite(connection).Options;

            var context = new AreaContext(option);

            if (context != null)
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
            }

            return context;
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}
