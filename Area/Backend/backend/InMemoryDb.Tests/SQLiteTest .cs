﻿using backend.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using Xunit;

// Refer link
// http s://www.c-sharpcorner.com/article/unit-testing-with-inmemory-provider-and-sqlite-in-memory-database-in-efcore/

namespace InMemoryDb.Tests
{
    public class SQLiteTest
    {
        [Fact]
        public void Task_Add_With_Relation()
        {
            //Arrange    
            var factory = new ConnectionFactory();

            //Get the instance of BlogDBContext  
            var context = factory.CreateContextForSQLite();

            var area = new Area() { Id = 7, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };

            //Act    
            var data = context.Areas.Add(area);
            context.SaveChanges();

            //Assert    
            //Get the post count  
            var areaCount = context.Areas.Count();
            if (areaCount != 0)
            {
                Assert.Equal(6, areaCount);
            }

            //Get single post detail  
            var singleArea = context.Areas.FirstOrDefault();
            if (singleArea != null)
            {
                Assert.Equal("Living Room", singleArea.Name);
            }
        }

        //[Fact]
        //public void Task_Add_Without_Relation()
        //{
        //    //Arrange    
        //    var factory = new ConnectionFactory();

        //    //Get the instance of BlogDBContext  
        //    var context = factory.CreateContextForSQLite();

        //    var area = new Area() { Id = 6, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };

        //    //Act    
        //    var data = context.Areas.Add(area);

        //    //Assert   
        //    Assert.Throws<DbUpdateException>(() => context.SaveChanges());
        //    Assert.Empty(context.Areas.ToList());
        //}

        //[Fact]
        //public void Task_Add_With_Relation_Return_Exception()
        //{
        //    //Arrange    
        //    var factory = new ConnectionFactory();

        //    //Get the instance of BlogDBContext  
        //    var context = factory.CreateContextForSQLite();

        //    var area = new Area() { Id = 6, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };

        //    //Act    
        //    var data = context.Areas.Add(area);

        //    //Assert   
        //    Assert.Throws<DbUpdateException>(() => context.SaveChanges());
        //    Assert.Empty(context.Areas.ToList());
        //}

        //[Fact]
        //public void Task_Add_With_Relation_Return_No_Exception()
        //{
        //    //Arrange    
        //    var factory = new ConnectionFactory();

        //    //Get the instance of BlogDBContext  
        //    var context = factory.CreateContextForSQLite();
        //    var area = new Area() { Id = 6, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };

        //    //Act    
        //    for (int i = 1; i < 4; i++)
        //    {
        //        var category = new Area() { Id = i, Name = "Living Room", CreatedOn = DateTime.UtcNow, CreatedById = 1, UpdatedOn = DateTime.UtcNow, UpdatedById = 1 };
        //        context.Areas.Add(category);
        //    }
        //    context.SaveChanges();

        //    var data = context.Areas.Add(area);
        //    context.SaveChanges();

        //    //Assert             

        //    //Get the Area count  
        //    var areaCount = context.Areas.Count();
        //    if (areaCount != 0)
        //    {
        //        Assert.Equal(1, areaCount);
        //    }

        //    //Get single Area detail  
        //    var singleArea = context.Areas.FirstOrDefault();
        //    if (singleArea != null)
        //    {
        //        Assert.Equal("Test Title 3", singleArea.Name);
        //    }
        //}

        //[Fact]
        //public void Task_Add_Time_Test()
        //{
        //    //Arrange    
        //    var factory = new ConnectionFactory();

        //    //Get the instance of BlogDBContext  
        //    var context = factory.CreateContextForInMemory();

        //    //Act   
        //    for (int i = 1; i < 4; i++)
        //    {
        //        var category = new Category() { Id = i, Name = "Category " + i, Slug = "slug" + i };
        //        context.Category.Add(category);
        //    }

        //    context.SaveChanges();

        //    for (int i = 1; i <= 1000; i++)
        //    {
        //        var Area = new Area() { Title = "Test Title " + i, Description = "Test Description " + i, CategoryId = 2, CreatedDate = DateTime.Now };
        //        context.Area.Add(Area);
        //    }

        //    context.SaveChanges();

        //    //Assert    
        //    //Get the Area count  
        //    var AreaCount = context.Area.Count();
        //    if (AreaCount != 0)
        //    {
        //        Assert.Equal(1000, AreaCount);
        //    }

        //    //Get single Area detail  
        //    var singleArea = context.Area.Where(x => x.AreaId == 1).FirstOrDefault();
        //    if (singleArea != null)
        //    {
        //        Assert.Equal("Test Title 1", singleArea.Title);
        //    }
        //}
    }
}
