﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using backend.Models;
using Newtonsoft.Json;

namespace backend.BusinessDomain
{
    public interface IS3Reader
    {
        Order ReadS3();
    }

    public class S3Reader : IS3Reader
    {
        static string bucketName = "ticketek-test-coding";
        static string keyName = "sampleOrder.json";
        static IAmazonS3 client;

        public Order ReadS3()
        {
            if (checkRequiredFields())
            {
                using (client = new AmazonS3Client())
                {
                    Console.WriteLine("Reading an object");
                    return ReadingAnObject();
                }
            }
            return null;

        }

        private bool checkRequiredFields()
        {
            NameValueCollection appConfig = ConfigurationManager.AppSettings;

            if (string.IsNullOrEmpty(appConfig["AWSProfileName"]))
            {
                Console.WriteLine("AWSProfileName was not set in the App.config file.");
                return false;
            }
            if (string.IsNullOrEmpty(bucketName))
            {
                Console.WriteLine("The variable bucketName is not set.");
                return false;
            }
            if (string.IsNullOrEmpty(keyName))
            {
                Console.WriteLine("The variable keyName is not set.");
                return false;
            }

            return true;
        }

        private Order ReadingAnObject()
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest()
                {
                    BucketName = bucketName,
                    Key = keyName
                };

                using (GetObjectResponse response = client.GetObject(request))
                {
                    string title = response.Metadata["x-amz-meta-title"];
                    using (Stream responseStream = response.ResponseStream)
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        var s3data = reader.ReadToEnd();
                        var order = JsonConvert.DeserializeObject<Order>(s3data);

                        return order;
                    }
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}