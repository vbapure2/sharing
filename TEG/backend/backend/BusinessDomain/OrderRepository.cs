﻿using backend.Context;
using backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backend.BusinessDomain
{
    public interface IOrderRepository
    {
        void InsertOrderHeader(OrderHeader orderHeader);
        void InsertOrderLines(List<OrderLine> orderlines);
        void InsertPackageLines(List<PackageLine> packagelines);
        OrderHeader GetAllOrderHeader();
        List<OrderLine> GetAllOrderLines();
        List<PackageLine> GetAllPackageLines();
    }

    public class OrderRepository : IOrderRepository
    {
        private OrderDbContext _orderHeadersContext;
        public OrderRepository(OrderDbContext orderHeadersContext)
        {
            _orderHeadersContext = orderHeadersContext;
        }

        public void InsertOrderHeader(OrderHeader orderHeader)
        {
            var hdrs = _orderHeadersContext.OrderHeaders.Where(_ => _.OrderId == orderHeader.OrderId);
            if(hdrs.Count() == 0)
            {
                _orderHeadersContext.OrderHeaders.Add(orderHeader);
            }
            _orderHeadersContext.SaveChanges();
        }

        public void InsertOrderLines(List<OrderLine> orderlines)
        {
            foreach(var orderline in orderlines)
            {
                var line = _orderHeadersContext.OrderLines.Where(_ => _.OrderLineId == orderline.OrderLineId);
                if(line.Count() == 0)
                {
                    _orderHeadersContext.OrderLines.Add(orderline);
                }
            }

            _orderHeadersContext.SaveChanges();
        }

        public void InsertPackageLines(List<PackageLine> packagelines)
        {
            foreach (var packageline in packagelines)
            {
                var line = _orderHeadersContext.PackageLines.Where(_ => _.PriceCategoryId == packageline.PriceCategoryId);
                if (line.Count() == 0)
                {
                    _orderHeadersContext.PackageLines.Add(packageline);
                }
                
            }

            _orderHeadersContext.SaveChanges();
        }

        

        public OrderHeader GetAllOrderHeader()
        {
            return _orderHeadersContext.OrderHeaders.First();
        }

        public List<OrderLine> GetAllOrderLines()
        {
            return _orderHeadersContext.OrderLines.ToList();
        }

        public List<PackageLine> GetAllPackageLines()
        {
            return _orderHeadersContext.PackageLines.ToList();
        }

    }
}