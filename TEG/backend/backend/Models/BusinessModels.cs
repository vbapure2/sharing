﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace backend.Models
{
    public class Order
    {
        public string _id { get; set; }
        public string orderId { get; set; }
        public string basketId { get; set; }
        public string dateTime { get; set; }
        public Customer customer { get; set; }
        public string seller { get; set; }
        public string chain { get; set; }
        public int channel { get; set; }
        public string productId { get; set; }
        public string productType { get; set; }
        public List<OrderLineItems> orderlineItems { get; set; }
    }

    public class Customer
    {
        public int _id { get; set; }
        public int organisationCustomerId { get; set; }
    }

    public class OrderLineItems
    {
        public string _id { get; set; }
        public int sign { get; set; }
        public Inventory inventory { get; set; }
    }

    public class Inventory
    {
        public string type { get; set; }
        public Price price { get; set; }
        public List<Packages> packages { get; set; }

    }

    public class Price
    {
        public int _id { get; set; }
        public int sheetId { get; set; }
        public string offerCode { get; set; }
        public int price { get; set; }
        public int gst { get; set; }
        public string gstRate { get; set; }
    }

    public class Packages
    {
        public InventorySource inventorySource { get; set; }
        public int priceCategoryId { get; set; }
        public string priceCategoryCode { get; set; }
        public string priceCategoryName { get; set; }
        public int priceTypeId { get; set; }
        public string priceTypeCode { get; set; }
        public string priceTypeName { get; set; }
        public bool nameOnTicket { get; set; }
        public string section { get; set; }
        public string row { get; set; }
        public string name { get; set; }
        public bool obstructed { get; set; }
        public string aisle { get; set; }
        public string barrier { get; set; }
        public string sourceCode { get; set; }
        public bool concession { get; set; }
        public string barcode { get; set; }
        public string paxNumber { get; set; }
        public string paxCardType { get; set; }
        public string paxEntryTime { get; set; }
        public CustomerOrder customer { get; set; }
    }

    public class InventorySource
    {
        public string _id { get; set; }
        public string name { get; set; }
    }

    public class CustomerOrder
    {
        public int _id { get; set; }
        public string afile { get; set; }
        public int account { get; set; }
        public int organisationCustomerId { get; set; }
    }
}