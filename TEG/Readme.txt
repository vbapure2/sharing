===================================================================================
This has backend (Web Api hosted on IIS) and frontend (Angular) folder

backend is webApi hosted on local IIS
	Reads Json data from S3 data storage
	Persists the read Json data to SQL Server
	Controller will return data from SQL Server when hit by Web Api

How to run backend?
	Build the solution
	Publish the solution to local IIS (It is already configured)
	Run WebApi http://localhost:8080/api/order

	This will return SQL Server data 

How to run frontend?
	1) npm install
	2) npm run start 
				This will issue a webApi request to get the data from backend
	3) Run http://localhost:4024/
				This will display data in front end table

Front end is assuming that backend is running on port 8080.
If backend is running on different port, open file frontend/proxy.conf.json
change target to whatever port backend is listening

	Example: proxy.conf.json
	{
    "/api/*": {
      "target": "http://localhost:8080/",	----- Change this port to point to backend port
      "secure": false,
      "logLevel": "debug",
      "pathRewrite": {
        "^/api": "api"
      },
      "changeOrigin": true
    }
  }

===================================================================================
Current Situation:
	I am not able to connect to S3 using given credentails, therefore I have harded coded values to simulate JSON data from S3
	These values are persisted to SQL Server

	Yesterday I was able to connect in your office, but after coming home I was not able to connect to S3

	We have to 
		Uncomment code in 
			File: BusinessDoman/BusinessService.cs
			Method: UpdateS3ToPersistence()

			That reads from S3

		Comment code:
			That forms temporary structures to persist to database	

===================================================================================
General Flow:
===================================================================================
front end (Angular)
   This will hit Web Api of backend
   localhost:8080/api/order

   Reads BusinessViewModel(OrderHeader, OrderLines, PackageLines)
   Displays this data in tabular format

backend (WebApi)

	Controller (IBusinessService)

	BusinessService(IS3Reader, IOrderRepository) 
		Reads S3 Json data
		Persists to SQL Server

	OrderRepository(OrderDbContext)
		Interacts with DBContext to persist to SQL Server and read data from SQL Server

	Models
		BusinessViewModel(OrderHeader, OrderLines, PackageLines) -- view model that is returned to front end
		BusinessModels	-- Reads JSON Data from S3 and stores in these structures
		DbModels -- models that are used to persist to SQL Server

===================================================================================
Not handled due to time constraints:
===================================================================================
  Error and Exceptions in backend
  Dependency Injection using containers like Autofac 
  			IBusinessService will be injected in Controller using Autofac
  			IS3Reader, IOrderRepository will be inejcted into BusinessService using Autofac
  Unit testing

===================================================================================
Design:
===================================================================================
  Web Api Controller just calls BusinessService object to process business logic


  Business Service handles all business logic by using IS3Reader and IOrderRepository

  IS3Reader, IOrderRepository -- are injected so that they can be unit tested

