﻿using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using bettinghost;
using System.Linq;
using Xunit;

namespace bettingTests
{
    public class bettingTest
    {
        [Theory]
        [InlineAutoMoqData("Bet:W:2:3")]
        [InlineAutoMoqData("Bet:K:2:3")]
        [InlineAutoMoqData("Bet:W    :        2:3")]
        [InlineAutoMoqData("Bet:W:3.32:3")]
        public void ReadInputBet_WinProduct_Test(string bet)
        {
            var sut = new InputBetsAndResult();
            var isValid = sut.ReadInputBet(bet);

            if(isValid)
            {
                var actual = sut.WinInputBets.Last();

                var tokens = bet.Split(':');
                var expectedProduct = Product.WIN;
                var expectedSelection = int.Parse(tokens[2].Trim(' '));
                var expectedStake = decimal.Parse(tokens[3].Trim(' '));

                Assert.Equal((int)expectedProduct, (int)actual.ProductType);
                Assert.Equal(expectedSelection, actual.Selection);
                Assert.Equal(expectedStake, actual.Stake);
            }
        }

        [Theory]
        [InlineAutoMoqData(2, 3, 1)]
        [InlineAutoMoqData(3, 2, 1)]
        [InlineAutoMoqData(2, 1, 3)]
        [InlineAutoMoqData(3, 1, 2)]
        [InlineAutoMoqData(1, 3, 2)]
        [InlineAutoMoqData(1, 2, 3)]
        public void DividendForWinningSelection_WinBet_Test(int first, int second, int third)
        {
            var inputBetsAndResult = ReadInputBetsandResult();
            inputBetsAndResult.Result.First = first;
            inputBetsAndResult.Result.Second = second;
            inputBetsAndResult.Result.Third = third;

            ICalculator sut = new WinCalculator(new WinCalculatorConfig(), inputBetsAndResult);

            var dividend = sut.DividendOfWinningSelection();

            var expectedDividend = CalculateDividendForWinBet(inputBetsAndResult);

            Assert.Equal(expectedDividend.D1, dividend.D1);
        }

        [Theory]
        [InlineAutoMoqData(2, 3, 1)]
        [InlineAutoMoqData(3, 2, 1)]
        [InlineAutoMoqData(2, 1, 3)]
        [InlineAutoMoqData(3, 1, 2)]
        [InlineAutoMoqData(1, 3, 2)]
        [InlineAutoMoqData(1, 2, 3)]
        public void DividendForWinningSelection_PlaceBet_Test(int first, int second, int third)
        {
            var inputBetsAndResult = ReadInputBetsandResult();
            inputBetsAndResult.Result.First = first;
            inputBetsAndResult.Result.Second = second;
            inputBetsAndResult.Result.Third = third;

            ICalculator sut = new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult);

            var dividend = sut.DividendOfWinningSelection();

            var expectedDividend = CalculateDividendForPlaceBet(inputBetsAndResult);

            Assert.Equal(expectedDividend.D1, dividend.D1);
            Assert.Equal(expectedDividend.D2, dividend.D2);
            Assert.Equal(expectedDividend.D3, dividend.D3);
        }

        [Theory]
        [InlineAutoMoqData(2, 3, 1)]
        [InlineAutoMoqData(3, 2, 1)]
        [InlineAutoMoqData(2, 1, 3)]
        [InlineAutoMoqData(3, 1, 2)]
        [InlineAutoMoqData(1, 3, 2)]
        [InlineAutoMoqData(1, 2, 3)]
        public void DividendForWinningSelection_ExactaBet_Test(int first, int second, int third)
        {
            var inputBetsAndResult = ReadInputBetsandResult();
            inputBetsAndResult.Result.First = first;
            inputBetsAndResult.Result.Second = second;
            inputBetsAndResult.Result.Third = third;

            ICalculator sut = new ExactaCalculator(new ExactaCalculatorConfig(), inputBetsAndResult);

            var dividend = sut.DividendOfWinningSelection();

            var expectedDividend = CalculateDividendForExactaBet(inputBetsAndResult);

            Assert.Equal(expectedDividend.D1, dividend.D1);
        }

        [Theory]
        [InlineAutoMoqData(2, 3, 0)]
        [InlineAutoMoqData(2, 3, 1)]
        [InlineAutoMoqData(3, 2, 1)]
        [InlineAutoMoqData(2, 1, 3)]
        [InlineAutoMoqData(3, 1, 2)]
        [InlineAutoMoqData(1, 3, 2)]
        [InlineAutoMoqData(1, 2, 3)]
        [InlineAutoMoqData(0, 1, 2)]
        [InlineAutoMoqData(3, 0, 2)]
        [InlineAutoMoqData(2, 2, 1)]
        public void BettingCalculator_Test(int first, int second, int third)
        {
            var inputBetsAndResult = ReadInputBetsandResult();
            inputBetsAndResult.Result.First = first;
            inputBetsAndResult.Result.Second = second;
            inputBetsAndResult.Result.Third = third;

            var sut = new BettingCalculator(new WinCalculator(new WinCalculatorConfig(), inputBetsAndResult),
                                            new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult),
                                            new ExactaCalculator(new ExactaCalculatorConfig(), inputBetsAndResult));

            var dividend = sut.DividendOfWinningSelection();

            var expectedWinDividend = CalculateDividendForWinBet(inputBetsAndResult);
            var expectedPlaceDividend = CalculateDividendForPlaceBet(inputBetsAndResult);
            var expectedExactaDividend = CalculateDividendForExactaBet(inputBetsAndResult);

            Assert.Equal(expectedWinDividend.D1, dividend.WinDividend.D1);
            Assert.Equal(expectedPlaceDividend.D1, dividend.PlaceDividend.D1);
            Assert.Equal(expectedPlaceDividend.D2, dividend.PlaceDividend.D2);
            Assert.Equal(expectedPlaceDividend.D3, dividend.PlaceDividend.D3);
            Assert.Equal(expectedExactaDividend.D1, dividend.ExactaDividend.D1);
        }

        [Fact]
        public void GetTotalOfPlaceBets()
        {
            var inputBetsAndResult = ReadInputBetsandResult();

            var sut = new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult);
            var actual = sut.GetTotal();
            var expected = inputBetsAndResult.PlaceInputBets.Sum(_ => _.Stake);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCommissionAmount()
        {
            var inputBetsAndResult = ReadInputBetsandResult();

            var sut = new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult);

            var actual = sut.GetCommissionAmount();

            var poolTotal = inputBetsAndResult.PlaceInputBets.Sum(_ => _.Stake);
            var expected = (double)(poolTotal * 12) / 100;

            Assert.Equal((decimal)expected, actual);
        }

        [Fact]
        public void RemainingAmountInPool()
        {
            var inputBetsAndResult = ReadInputBetsandResult();

            var sut = new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult);

            var actual = sut.RemainingAmountInPool();

            var poolTotal = inputBetsAndResult.PlaceInputBets.Sum(_ => _.Stake);
            var expected = (decimal)((double)poolTotal - ((double)(poolTotal * 12) / 100));

            Assert.Equal(expected, (decimal)actual);
        }

        [Fact]
        public void RemainingAmountForEachPlace()
        {
            var inputBetsAndResult = ReadInputBetsandResult();

            var sut = new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult);

            var actual = sut.RemainingAmountForEachPlace();

            var poolTotal = inputBetsAndResult.PlaceInputBets.Sum(_ => _.Stake);
            var remainingTotal = (decimal)((double)poolTotal - ((double)(poolTotal * 12) / 100));
            var expected = decimal.Round((decimal)(remainingTotal / 3), 2);

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void WageredAmount_PlaceBet_Test()
        {
            var inputBetsAndResult = ReadInputBetsandResult();

            var sut = new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult);

            var actual = sut.WageredAmount(inputBetsAndResult.Result);

            var expected = inputBetsAndResult
                        .PlaceInputBets
                        .Where(_ => _.Selection == inputBetsAndResult.Result.First)
                        .Sum(_ => _.Stake);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WageredAmount_ExactaBet_Test()
        {
            var inputBetsAndResult = ReadInputBetsandResult();

            var sut = new ExactaCalculator(new ExactaCalculatorConfig(), inputBetsAndResult);

            var actual = sut.WageredAmount(inputBetsAndResult.Result);

            var expected = inputBetsAndResult
                        .ExactaInputBets
                        .Where(_ => _.Selection == inputBetsAndResult.Result.First
                                  && _.Selection2 == inputBetsAndResult.Result.Second)
                        .Sum(_ => _.Stake);

            Assert.Equal(expected, actual);
        }

        private InputBetsAndResult ReadInputBetsandResult()
        {
            var inputBetsAndResult = new InputBetsAndResult();

            inputBetsAndResult.ReadInputBet("Bet:P:1:31");
            inputBetsAndResult.ReadInputBet("Bet:P:1:40");
            inputBetsAndResult.ReadInputBet("Bet:P:1:18");

            inputBetsAndResult.ReadInputBet("Bet:P:2:89");
            inputBetsAndResult.ReadInputBet("Bet:P:2:16");
            inputBetsAndResult.ReadInputBet("Bet:P:2:74");

            inputBetsAndResult.ReadInputBet("Bet:P:3:28");
            inputBetsAndResult.ReadInputBet("Bet:P:3:82");
            inputBetsAndResult.ReadInputBet("Bet:P:3:39");

            inputBetsAndResult.ReadInputBet("Bet:P:4:72");
            inputBetsAndResult.ReadInputBet("Bet:P:4:52");
            inputBetsAndResult.ReadInputBet("Bet:P:4:105");

            inputBetsAndResult.ReadInputBet("Bet:E:1,2:13");
            inputBetsAndResult.ReadInputBet("Bet:E:2,3:98");
            inputBetsAndResult.ReadInputBet("Bet:E:1,3:82");

            inputBetsAndResult.ReadInputBet("Bet:E:3,2:27");
            inputBetsAndResult.ReadInputBet("Bet:E:1,2:5");
            inputBetsAndResult.ReadInputBet("Bet:E:2,3:61");

            inputBetsAndResult.ReadInputBet("Bet:E:1,3:28");
            inputBetsAndResult.ReadInputBet("Bet:E:3,2:25");
            inputBetsAndResult.ReadInputBet("Bet:E:1,2:81");

            inputBetsAndResult.ReadInputBet("Bet:E:2,3:47");
            inputBetsAndResult.ReadInputBet("Bet:E:1,3:93");
            inputBetsAndResult.ReadInputBet("Bet:E:3,2:51");

            inputBetsAndResult.ReadInputBet("Bet:W:1:3.12");
            inputBetsAndResult.ReadInputBet("Bet:W:2:41.32");
            inputBetsAndResult.ReadInputBet("Bet:W:3:50.33");

            inputBetsAndResult.ReadInputBet("Bet:W:4:5");
            inputBetsAndResult.ReadInputBet("Bet:W:2:8");
            inputBetsAndResult.ReadInputBet("Bet:W:3:22");

            inputBetsAndResult.ReadInputBet("Bet:W:4:57");
            inputBetsAndResult.ReadInputBet("Bet:W:1:42");
            inputBetsAndResult.ReadInputBet("Bet:W:2:98");

            inputBetsAndResult.ReadInputBet("Bet:W:3:63");
            inputBetsAndResult.ReadInputBet("Bet:W:4:15");
            inputBetsAndResult.ReadInputBet("Bet:W:1:16");

            inputBetsAndResult.ReadResult("Result:3:2:1");

            return inputBetsAndResult;
        }

        private Dividend CalculateDividendForPlaceBet(InputBetsAndResult inputBetsAndResult)
        {
            var poolTotal = inputBetsAndResult.PlaceInputBets.Sum(_ => _.Stake);

            var remainingTotal = (decimal)((double)poolTotal - ((double)(poolTotal * 12) / 100));

            var dividedTotal = decimal.Round((decimal)(remainingTotal / 3), 2);

            var t1 = inputBetsAndResult
                        .PlaceInputBets
                        .Where(_ => _.Selection == inputBetsAndResult.Result.First)
                        .Sum(_ => _.Stake);

            var t2 = inputBetsAndResult
                        .PlaceInputBets
                        .Where(_ => _.Selection == inputBetsAndResult.Result.Second)
                        .Sum(_ => _.Stake);

            var t3 = inputBetsAndResult
                        .PlaceInputBets
                        .Where(_ => _.Selection == inputBetsAndResult.Result.Third)
                        .Sum(_ => _.Stake);
            
            var d1 = t1 == 0 ? 0 : decimal.Round(dividedTotal / t1, 2);
            var d2 = t2 == 0 ? 0 : decimal.Round(dividedTotal / t2, 2);
            var d3 = t3 == 0 ? 0 : decimal.Round(dividedTotal / t3, 2);

            return new Dividend(d1, d2, d3);
        }

        private Dividend CalculateDividendForExactaBet(InputBetsAndResult inputBetsAndResult)
        {
            var poolTotal = inputBetsAndResult.ExactaInputBets.Sum(_ => _.Stake);

            var remainingTotal = (decimal)((double)poolTotal - ((double)(poolTotal * 18) / 100));

            var t1 = inputBetsAndResult
                        .ExactaInputBets
                        .Where(_ => _.Selection == inputBetsAndResult.Result.First 
                                    && _.Selection2 == inputBetsAndResult.Result.Second)
                        .Sum(_ => _.Stake);

            if(t1 == 0)
            {
                return new Dividend(0);
            }
            var d1 = decimal.Round(remainingTotal / t1, 2);

            return new Dividend(d1);
        }

        private Dividend CalculateDividendForWinBet(InputBetsAndResult inputBetsAndResult)
        {
            var poolTotal = inputBetsAndResult.WinInputBets.Sum(_ => _.Stake);

            var remainingTotal = (decimal)((double)poolTotal - ((double)(poolTotal * 15) / 100));

            var t1 = inputBetsAndResult
                        .WinInputBets
                        .Where(_ => _.Selection == inputBetsAndResult.Result.First)
                        .Sum(_ => _.Stake);
            if (t1 == 0)
            {
                return new Dividend(0);
            }

            var d1 = decimal.Round(remainingTotal / t1, 2);

            return new Dividend(d1);
        }
    }

    public class InlineAutoMoqDataAttribute : CompositeDataAttribute
    {
        public InlineAutoMoqDataAttribute(params object[] values)
            : base(new InlineDataAttribute(values), new AutoMoqDataAttribute())
        {
        }
    }

    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute()
            : base(() => new Fixture().Customize(new AutoMoqCustomization()))
        {
        }
    }
}
