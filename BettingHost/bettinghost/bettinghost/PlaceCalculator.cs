﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bettinghost
{
    public class PlaceCalculator : ICalculator
    {
        //public const int COMMISSIONPERCENTAGE = 12;
        private int commission { get; set; }
        public InputBetsAndResult InputBetsAndResult { get; private set; }

        public PlaceCalculator(CalculatorConfig config, InputBetsAndResult inputBetsAndResult)
        {
            InputBetsAndResult = inputBetsAndResult;
            commission = config.COMMISSIONPERCENTAGE;
        }

        public decimal GetTotal()
        {
            return InputBetsAndResult.PlaceInputBets.Sum(_ => _.Stake);
        }

        public decimal GetCommissionAmount()
        {
            var commissionAmount = (double)(commission * GetTotal()) /  100;
            return (decimal)commissionAmount;
        }

        public decimal RemainingAmountInPool()
        {
            var remainingAmountInPool = GetTotal() - GetCommissionAmount();

            return remainingAmountInPool;
        }

        public decimal RemainingAmountForEachPlace()
        {
            return decimal.Round((decimal)(RemainingAmountInPool() / 3), 2);
        }

        public decimal WageredAmount(Result result)
        {
            var horseWaggered = (decimal)0.00;

            horseWaggered = InputBetsAndResult
                .PlaceInputBets
                .Where(_ => _.Selection == result.First)
                .Sum(_ => _.Stake);

            return horseWaggered;
        }

        private decimal DividendOfHorsePosition(int horsePosition)
        {
            var horseWaggered = WageredAmount(new Result(horsePosition, 0, 0));

            if (horseWaggered == 0)
            {
                //Console.WriteLine("PLACE Race -- No Amount/Place Bet on Winning Horse: {0}", horsePosition);
                return 0;
            }

            var dividend = decimal.Round(RemainingAmountForEachPlace() / horseWaggered, 2);
            return dividend;
        }

        public Dividend DividendOfWinningSelection()
        {
            return new Dividend(DividendOfHorsePosition(InputBetsAndResult.Result.First),
                                DividendOfHorsePosition(InputBetsAndResult.Result.Second),
                                DividendOfHorsePosition(InputBetsAndResult.Result.Third));
        }
    }
}
