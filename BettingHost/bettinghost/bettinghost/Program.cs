﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bettinghost
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter list of Bets: ");
            Console.WriteLine("Enter one bet per line in the format Bet:<product>:<selections>:<stake>");
            //var bet = Console.ReadLine();
            //Console.WriteLine("You entered = {0}", bet);
            const int INPUTBET = 1;
            const int RESULT = 2;

            var inputBetsAndResult = new InputBetsAndResult();

            var isInputBet = INPUTBET;
            while(isInputBet == INPUTBET)
            {
                var valid = false;
                while (!valid)
                {
                    Console.WriteLine("Please enter input bet");
                    var bet = Console.ReadLine();
                    valid = inputBetsAndResult.ReadInputBet(bet);
                }


                var isInputBetResult = "0";
                while(isInputBetResult != "1" && isInputBetResult != "2")
                {
                    Console.WriteLine("Press 1 to enter next input bet. Press 2 to enter Result");
                    isInputBetResult = Console.ReadLine();
                }

                isInputBet = int.Parse(isInputBetResult);
                
            }

            if(isInputBet == RESULT)
            {
                Console.WriteLine("Press enter Result in the format -- Result:<first>:<second>:<third>");

                var valid = false;
                while(!valid)
                {
                    var result = Console.ReadLine();
                    valid = inputBetsAndResult.ReadResult(result);
                }
            }

            var calculator = new BettingCalculator(new WinCalculator(new WinCalculatorConfig(), inputBetsAndResult),
                                            new PlaceCalculator(new PlaceCalculatorConfig(), inputBetsAndResult),
                                            new ExactaCalculator(new ExactaCalculatorConfig(), inputBetsAndResult));

            var dividend = calculator.DividendOfWinningSelection();

            inputBetsAndResult.PrintOutputDividends(dividend);            

            Console.ReadLine();
        }

        

    }
}
