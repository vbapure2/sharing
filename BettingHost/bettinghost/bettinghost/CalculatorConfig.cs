﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bettinghost
{
    public abstract class CalculatorConfig
    {
        public abstract int COMMISSIONPERCENTAGE { get; set; }
    }

    public class WinCalculatorConfig : CalculatorConfig
    {
        public override int COMMISSIONPERCENTAGE { get; set; }
        public WinCalculatorConfig()
        {
            COMMISSIONPERCENTAGE = 15;
        }
    }

    public class PlaceCalculatorConfig : CalculatorConfig
    {
        public override int COMMISSIONPERCENTAGE { get; set; }
        public PlaceCalculatorConfig()
        {
            COMMISSIONPERCENTAGE = 12;
        }
    }

    public class ExactaCalculatorConfig : CalculatorConfig
    {
        public override int COMMISSIONPERCENTAGE { get; set; }
        public ExactaCalculatorConfig()
        {
            COMMISSIONPERCENTAGE = 18;
        }
    }

}
