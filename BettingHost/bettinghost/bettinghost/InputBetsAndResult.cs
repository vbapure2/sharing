﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bettinghost
{
    public enum Product { WIN = 1, PLACE = 2, EXACTA = 3 };

    public class WinPlaceInputBet
    {
        public int ProductType { get; set; }
        public int Selection { get; set; }
        public decimal Stake { get; set; }

        public WinPlaceInputBet(int productType, int selection, decimal stake)
        {
            ProductType = productType;
            Selection = selection;
            Stake = stake;
        }
    }

    public class ExactaInputBet : WinPlaceInputBet
    {
        public int Selection2 { get; set; }

        public ExactaInputBet(int productType, int selection, int selection2, decimal stake) : base(productType, selection, stake)
        {
            Selection2 = selection2;
        }
    }

    public class Result
    {
        public int First { get; set; }
        public int Second { get; set; }
        public int Third { get; set; }

        public Result()
        {
            First = 0;
            Second = 0;
            Third = 0;
        }

        public Result(int first, int second, int third)
        {
            First = first;
            Second = second;
            Third = third;
        }
    }

    public class InputBetsAndResult
    {
        public List<WinPlaceInputBet> WinInputBets { get; set; }
        public List<WinPlaceInputBet> PlaceInputBets { get; set; }
        public List<ExactaInputBet> ExactaInputBets { get; set; }
        public Result Result { get; set; }

        public InputBetsAndResult()
        {
            WinInputBets = new List<WinPlaceInputBet>();
            PlaceInputBets = new List<WinPlaceInputBet>();
            ExactaInputBets = new List<ExactaInputBet>();
            Result = new Result();
        }

        public bool ReadInputBet(string inputBet)
        {
            bool isValid = true;
            if(!IsValidInputBet(inputBet))
            {
                isValid = false;
                return isValid;
            }

            var tokens = inputBet.Split(':');

            var productType = Convert.ToChar(tokens[1].Trim(' '));
            switch (productType)
            {
                case 'W':
                    WinInputBets.Add(new WinPlaceInputBet(
                        (int)Product.WIN,
                        int.Parse(tokens[2].Trim(' ')),
                        decimal.Parse(tokens[3].Trim(' '))));
                    break;
                case 'P':
                    PlaceInputBets.Add(new WinPlaceInputBet(
                        (int)Product.PLACE,
                        int.Parse(tokens[2].Trim(' ')),
                        decimal.Parse(tokens[3].Trim(' '))));
                    break;

                case 'E':
                    var selections = tokens[2].Split(',');
                    ExactaInputBets.Add(new ExactaInputBet((int)Product.EXACTA,
                        int.Parse(selections[0].Trim(' ')),
                        int.Parse(selections[1].Trim(' ')),
                        decimal.Parse(tokens[3].Trim(' '))));
                    break;
                default:
                    Console.WriteLine("Product Entered is Invalid: Please Enter one of Valid Product ['W', 'P', 'E']");
                    break;
            }

            return isValid;
        }

        private bool IsValidInputBet(string bet)
        {
            bool isValid = false;
            var tokens = bet.Split(':');

            var producttypes = new string[] { "W", "P", "E" };
            var product = tokens[1].Trim(' ');
            var validProduct = producttypes.Contains(product) ? true : false;

            if(validProduct)
            {
                try
                {
                    if(product == "E")
                    {
                        var selections = tokens[2].Split(',');

                        var selection1 = int.Parse(selections[0].Trim(' '));
                        var selection2 = int.Parse(selections[1].Trim(' '));
                    }
                    else
                    {
                        var validSelection = int.Parse(tokens[2].Trim(' '));
                    }

                    
                    try
                    {
                        var validStake = decimal.Parse(tokens[3].Trim(' '));
                        isValid = true;
                    }
                    catch(Exception)
                    {
                        Console.WriteLine("Invalid Stake. Please enter valid amount");
                    }
                }
                catch(Exception)
                {
                    Console.WriteLine("Invalid Selection. Please enter valid selection");
                }
            }
            else
            {
                Console.WriteLine("Product Entered is Invalid: Please Enter one of Valid Product ['W', 'P', 'E']");
            }

            return isValid;
        }

        public bool ReadResult(string result)
        {
            if(!IsValidResult(result))
            {
                return false;
            }
            var tokens = result.Split(':');

            Result.First = int.Parse(tokens[1].Trim(' '));
            Result.Second = int.Parse(tokens[2].Trim(' '));
            Result.Third = int.Parse(tokens[3].Trim(' '));

            return true;
        }

        private bool IsValidResult(string result)
        {
            bool isValid = true;
            var tokens = result.Split(':');

            try
            {
                if(tokens[0].Trim(' ') != "Result")
                {
                    Console.WriteLine("Please Enter Valid Result");
                    return false;
                }

                var first = int.Parse(tokens[1].Trim(' '));
                var second = int.Parse(tokens[2].Trim(' '));
                var third = int.Parse(tokens[3].Trim(' '));
            }
            catch(Exception)
            {
                Console.WriteLine("Please Enter Valid Result");
                isValid = false;
            }

            return isValid;
        }

        public void PrintOutputDividends(TotalDividend dividend)
        {
            var wd = "Win" + ":" + Result.First + ":" + "$" + dividend.WinDividend.D1;
            var pd1 = "Place" + ":" + Result.First + ":" + "$" + dividend.PlaceDividend.D1;
            var pd2 = "Place" + ":" + Result.Second + ":" + "$" + dividend.PlaceDividend.D2;
            var pd3 = "Place" + ":" + Result.Third + ":" + "$" + dividend.PlaceDividend.D3;
            var ed = "Exacta" + ":" + Result.First + "," + Result.Second + ":" + "$" + dividend.ExactaDividend.D1;

            if(dividend.WinDividend.D1 == 0)
            {
                Console.WriteLine("WIN Race -- No Amount Bet on Winning Selection");
            }
            Console.WriteLine(wd);

            if (dividend.PlaceDividend.D1 == 0)
            {
                Console.WriteLine("PLACE Race -- No Amount/Place Bet on Winning Horse: {0}", Result.First);
            }
            Console.WriteLine(pd1);

            if (dividend.PlaceDividend.D2 == 0)
            {
                Console.WriteLine("PLACE Race -- No Amount/Place Bet on Winning Horse: {0}", Result.Second);
            }

            Console.WriteLine(pd2);

            if (dividend.PlaceDividend.D3 == 0)
            {
                Console.WriteLine("PLACE Race -- No Amount/Place Bet on Winning Horse: {0}", Result.Third);
            }
            Console.WriteLine(pd3);

            if (dividend.ExactaDividend.D1 == 0)
            {
                Console.WriteLine("Exacta Race -- No Amount Bet on Winning Selection");
            }
            Console.WriteLine(ed);
        }

    }

    public class Dividend
    {
        public decimal D1 { get; set; }
        public decimal D2 { get; set; }
        public decimal D3 { get; set; }

        public Dividend(decimal d1)
        {
            D1 = d1;
            D2 = 0;
            D3 = 0;
        }


        public Dividend(decimal d1, decimal d2, decimal d3)
        {
            D1 = d1;
            D2 = d2;
            D3 = d3;
        }
    } 

    public class TotalDividend
    {
        public Dividend WinDividend { get; set; }
        public Dividend PlaceDividend { get; set; }
        public Dividend ExactaDividend { get; set; }

        public TotalDividend(Dividend winDividend, Dividend placeDividend, Dividend exactaDividend)
        {
            WinDividend = winDividend;
            PlaceDividend = placeDividend;
            ExactaDividend = exactaDividend;
        }
    }
}
