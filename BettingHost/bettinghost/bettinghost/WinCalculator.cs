﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bettinghost
{
    public class WinCalculator : ICalculator
    {
        // public const int COMMISSIONPERCENTAGE = 15;
        private int commission { get; set; }
        public InputBetsAndResult InputBetsAndResult { get; private set; }

        public WinCalculator(CalculatorConfig config, InputBetsAndResult inputBetsAndResult)
        {
            InputBetsAndResult = inputBetsAndResult;
            commission = config.COMMISSIONPERCENTAGE;
        }

        public decimal GetTotal()
        {
            return InputBetsAndResult.WinInputBets.Sum(_ => _.Stake);
        }

        public decimal GetCommissionAmount()
        {
            var commissionAmount = (double)(commission * GetTotal()) / 100;
            return (decimal)commissionAmount;
        }

        public decimal RemainingAmountInPool()
        {
            var remainingAmountInPool = GetTotal() - GetCommissionAmount();

            return remainingAmountInPool;
        }

        public decimal WageredAmount(Result result)
        {
            var horseWaggered = (decimal)0.00;

            horseWaggered = InputBetsAndResult
                .WinInputBets
                .Where(_ => _.Selection == result.First)
                .Sum(_ => _.Stake);

            return horseWaggered;
        }

        public Dividend DividendOfWinningSelection()
        {
            var horseWaggered = WageredAmount(InputBetsAndResult.Result);

            if (horseWaggered == 0)
            {
                // Console.WriteLine("WIN Race -- No Amount Bet on Winning Selection");
                return new Dividend(0);
            }

            var dividend = decimal.Round((decimal)RemainingAmountInPool() / horseWaggered, 2);
            return new Dividend(dividend);
        }
    }
}
