﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bettinghost
{
    public interface ICalculator
    {
        decimal GetTotal();
        decimal GetCommissionAmount();
        decimal RemainingAmountInPool();
        decimal WageredAmount(Result result);
        Dividend DividendOfWinningSelection();
    }
}
