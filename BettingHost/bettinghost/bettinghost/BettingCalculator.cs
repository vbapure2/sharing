﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bettinghost
{
    public class BettingCalculator
    {
        private ICalculator WinCalculator {get; set;}
        private ICalculator PlaceCalculator { get; set; }
        private ICalculator ExactaCalculator { get; set; }

        public BettingCalculator(ICalculator winBetCalculator, 
                                 ICalculator placeBetCalculator, 
                                 ICalculator exactaCalculator)
        {
            WinCalculator = winBetCalculator;
            PlaceCalculator = placeBetCalculator;
            ExactaCalculator = exactaCalculator;
        }

        public TotalDividend DividendOfWinningSelection()
        {
            var winDivedend = WinCalculator.DividendOfWinningSelection();
            var placeDividend = PlaceCalculator.DividendOfWinningSelection();
            var exactaDividend = ExactaCalculator.DividendOfWinningSelection();

            return new TotalDividend(winDivedend, placeDividend, exactaDividend);
        }
    }
}
