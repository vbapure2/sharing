import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TabService {
  params: HttpParams;

  constructor(private http: HttpClient) { 
    this.params = new HttpParams().set('jurisdiction', 'NSW');
  }

  getUpComingRaces(region: string) : Observable<any> {
    console.log('region: ', region);
    const par = new HttpParams().set('jurisdiction', region);
    console.log(par.toString());
    return this.http
                .get('/api/v1/tab-info-service/racing/next-to-go/races', {params: par});
  }
}
