import { Component, OnInit } from '@angular/core';
import { TabService } from './tab.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'bettingSite';

  selectedJurisdiction = 1;
  selectedRaceType: string = 'R';

  jurisdictions = [
    {id: 1, value: 'NSW'}, 
    {id: 2, value: 'VIC'}
  ];

  raceTypes = [
    {id: 'R', value : 'Thoroughbred'},
    {id: 'G', value: 'Greyhounds'},
    {id: 'H', value: 'Harness'}
  ];

  isLoadingResults = true;
  errorLoadingResults = false;

  upcomingRaces: any;
  races: any[];
  filteredRaces: any;
  

  constructor(private tabService: TabService) { }

  ngOnInit() {
    this.selectedRaceType = 'R';
    this.selectedJurisdiction = 1;
    this.upcomingRaces = this.fetchUpComingRaces(this.selectedJurisdiction);
  }

  regionChange(region: number) {
    console.log('regionChange: ', region);
    this.selectedJurisdiction = region;

    // Unsubscribe from existing obervable
    this.upcomingRaces.unsubscribe();

    this.upcomingRaces = this.fetchUpComingRaces(region);
    this.selectedRaceType = 'R';
  }

  fetchUpComingRaces(region: number): any {
    let jusrisdiction = this.jurisdictions.filter(_ => _.id == region).pop()
    return this.tabService
        .getUpComingRaces(jusrisdiction.value)
        .subscribe(
          (_) => {
            this.errorLoadingResults = false;
            this.races = [];
            this.races = _.races;
            this.filteredRaces = this.races
                                   .filter(_ => _.meeting.raceType == this.selectedRaceType)
                                   .map(_ => {
                                     return {
                                       "StartTime" : _.raceStartTime,
                                       "Number": _.raceNumber,
                                       "Name": _.raceName,
                                       "MeetingName": _.meeting.meetingName,
                                       "MeetingLocation": _.meeting.location,
                                       "MeetingDate": _.meeting.meetingDate
                                     }
                                   })
                                   .sort((a, b) => new Date(a.StartTime).getTime() - new Date(b.StartTime).getTime());
            this.isLoadingResults = false;
            console.log('filtered values: ', this.filteredRaces);
          },
          (err) => {
            this.isLoadingResults = false;
            this.errorLoadingResults = true;
            console.log('Error in service')
          });
  }

  raceTypeChange(raceType: string) {
    this.selectedRaceType = raceType;
    console.log('Race Type Change: ', this.selectedRaceType);
    this.filteredRaces = this.races
                            .filter(_ => _.meeting.raceType == raceType)
                            .map(_ => {
                              return {
                                "StartTime" : _.raceStartTime,
                                "Number": _.raceNumber,
                                "Name": _.raceName,
                                "MeetingName": _.meeting.meetingName,
                                "MeetingLocation": _.meeting.location,
                                "MeetingDate": _.meeting.meetingDate
                              }
                            });
    console.log('filteredRaces: ', this.filteredRaces);
  }
}
