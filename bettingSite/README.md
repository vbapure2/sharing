# BettingSite

## Install Node.js on your machine
## Install global angular cli
   npm install -g @angular/cli

## Download bettingSite folder
   Navigate to bettingSite directory on the command line

## Install dependend packages
   Run:
       npm install

   Outcome:
       Running above command should create node_modules directory and package-lock.json file

## Build
	Run command:
		npm run start

	Outcome:
		This should build angular project and run a angular development server

## Open browser (I used Chrome)
	Naviage to `http://localhost:4200/`

	The app will automatically reload if you change any of the source files.

## Access to TAB API using Angular CLI proxy configuration

  File 'proxy.conf.json' is created with below configuration
   {
    "/api/*": {
      "target": "https://api.beta.tab.com.au",
      "secure": false,
      "logLevel": "debug",
      "pathRewrite": {
        "^/api": ""
      },
      "changeOrigin": true
    }

   package.json is updated with proxy configuration information

   "scripts": {
    "start": "ng serve --proxy-config proxy.conf.json --host 0.0.0.0",
   }
  

