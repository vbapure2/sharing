﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csales.Models
{
    public class ResponseModel
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }

        public object ReturnObject { get; set; }

        public static ResponseModel Error(string errorMessage)
        {
            return new ResponseModel
            {
                IsSuccess = false,
                Message = errorMessage,
                ReturnObject = null
            };
        }
    }
}
