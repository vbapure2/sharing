﻿using csales.Models;
using Microsoft.AspNetCore.Mvc;

namespace csales.Businesses
{
    public interface ICommonService
    {
        ResponseModel GetVehicles(string vehicleType);
    }
}
