﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csales.Models;
using csales.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace csales.Businesses
{
    public class CommonService: ICommonService
    {
        private ICommonRepository _commonRepository;
        public CommonService(ICommonRepository commonRepository)
        {
            _commonRepository = commonRepository;
        }

        public ResponseModel GetVehicles(string vehicleType)
        {
            var response = new ResponseModel();

            try
            {
                return _commonRepository.GetVehicles(vehicleType);

            }
            catch (Exception ex)
            {
                Helper.LogError(ex.ToString());
                response.IsSuccess = false;
                response.Message = "An Exception Occured. Please Contact Administrator";
                return response;
            }
        }
    }
}
