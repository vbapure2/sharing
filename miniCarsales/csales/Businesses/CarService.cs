﻿using System;
using csales.Models;
using csales.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace csales.Businesses
{
    public class CarService : ICarService
    {
        private ICarRepository _carRepository;
        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public ResponseModel CreateCar(CarModel carModel)
        {
            var response = new ResponseModel();

            try
            {
               return  _carRepository.CreateCar(carModel);

            }
            catch(Exception ex)
            {
                Helper.LogError(ex.ToString());
                response.IsSuccess = false;
                response.Message = "An Exception Occured. Please Contact Administrator";
                return response;
            }
        }

      
    }
}
