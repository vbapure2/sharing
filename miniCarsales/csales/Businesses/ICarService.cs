﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csales.Models;
using Microsoft.AspNetCore.Mvc;

namespace csales.Businesses
{
    public interface ICarService
    {
        ResponseModel CreateCar(CarModel carModel);
        
    }
}
