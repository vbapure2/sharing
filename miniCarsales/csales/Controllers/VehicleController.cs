﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csales.Businesses;
using csales.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace csales.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : Controller
    {
        private ICommonService _commonService;
        private ICarService _carService;

        public VehicleController(ICommonService commonService, ICarService carService)
        {
            _commonService = commonService;
            _carService = carService;
        }

        [HttpGet, Route("getVehicles/{vehicleType}")]
        public ActionResult<ResponseModel> GetVehicles(string vehicleType)
        {
            return _commonService.GetVehicles(vehicleType);
        }

        [HttpPost, Route("createCar")]
        public ActionResult<ResponseModel> CreateCar([FromBody]CarModel carModel)
        {
            return _carService.CreateCar(carModel);
        }
    }
}