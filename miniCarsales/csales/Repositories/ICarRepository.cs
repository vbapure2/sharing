﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csales.Models;

namespace csales.Repositories
{
    public interface ICarRepository
    {
        ResponseModel CreateCar(CarModel carModel);
    }
}
