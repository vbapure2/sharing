﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csales.Repositories
{
        public abstract class BaseRepository
        {
            protected VehicleDbContext Context
            {
                get; private set;
            }

            public BaseRepository(VehicleDbContext context)
            {
                Context = context;
            }
        }
}
