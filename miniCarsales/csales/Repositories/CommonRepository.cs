﻿using System.Linq;
using csales.Models;
using csales.Repositories.Entities;
using csales.Repositories.Mapping;
using Microsoft.AspNetCore.Mvc;

namespace csales.Repositories
{
    public class CommonRepository : BaseRepository, ICommonRepository
    {
        public CommonRepository(VehicleDbContext context) : base(context)
        { }

        public ResponseModel GetVehicles(string vehicleType)
        {
            var response = new ResponseModel();

            var entities = Context.Set<CarEntity>().Where(x =>x.Type.Equals(vehicleType));
            if (entities != null && entities.Count() > 0)
            {
                response.IsSuccess = true;
                response.Message = "Cars Found.";
                response.ReturnObject = entities.MapToModels();
                return response;
            }

            response.IsSuccess = false;
            response.Message = "No Vehicles Found";

            return response;
        }
    }
}
