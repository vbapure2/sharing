﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace csales.Repositories
{
    public static class Helper
    {
        public static void LogError(object error)
        {
            System.Diagnostics.Trace.WriteLine($"{DateTime.Now} ERROR :");
            System.Diagnostics.Trace.WriteLine(error);

            System.Diagnostics.Debug.WriteLine($"{DateTime.Now} ERROR :");
            System.Diagnostics.Debug.WriteLine(error);
        }
        public static void LogInfo(object info)
        {
            System.Diagnostics.Trace.WriteLine($"{DateTime.Now} INFO :");
            System.Diagnostics.Trace.WriteLine(info);

            System.Diagnostics.Debug.WriteLine($"{DateTime.Now} INFO :");
            System.Diagnostics.Debug.WriteLine(info);
        }
    }
}
