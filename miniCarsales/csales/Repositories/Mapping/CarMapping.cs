﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using csales.Models;
using csales.Repositories.Entities;

namespace csales.Repositories.Mapping
{
    public static partial class MappingHelper
    {
        public static CarEntity MapToEntity(this CarModel carModel)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<CarModel, CarEntity>());
            var mapper = new Mapper(config);

            return mapper.Map<CarEntity>(carModel);
        }

        public static IEnumerable<CarModel> MapToModels(this IEnumerable<CarEntity> entities)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<CarEntity, CarModel>());
            var mapper = new Mapper(config);

            var models = new List<CarModel>();
            foreach (var dbEntity in entities)
            {
                var model = mapper.Map<CarModel>(dbEntity);
                models.Add(model);
            }

            return models;
        }
       
    }
}
