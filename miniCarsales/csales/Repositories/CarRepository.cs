﻿using System.Linq;
using AutoMapper;
using csales.Models;
using csales.Models.Constants;
using csales.Repositories.Entities;
using csales.Repositories.Mapping;

namespace csales.Repositories
{
    public class CarRepository: BaseRepository, ICarRepository
    {
        public CarRepository(VehicleDbContext context) : base(context)
        {}

        public ResponseModel CreateCar(CarModel carModel)
        {
            var response = new ResponseModel();
                      
            var ent = Context.Set<CarEntity>().Where(x => x.Id == carModel.Id && x.Type.Equals(VehicleTypes.CAR)).FirstOrDefault();
            if (ent != null)
            {
                response.IsSuccess = false;
                response.Message = "Car Already Exists for given ID. Please Create a New Car.";
                return response;
            }

            var entity = carModel.MapToEntity();
            Context.Cars.Add(entity);
            Context.SaveChanges();

            response.IsSuccess = true;
            response.Message = "Create Car Successful";
            return response;
            
        }
        
    }
}
