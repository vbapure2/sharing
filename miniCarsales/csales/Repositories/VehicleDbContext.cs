﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using csales.Repositories.Entities;
using Microsoft.EntityFrameworkCore;

namespace csales.Repositories
{
    public class VehicleDbContext: DbContext
    {
        public DbSet<CarEntity> Cars { get; set; }

        public VehicleDbContext(DbContextOptions<VehicleDbContext> options): base(options)
        {
        }
    }
}
