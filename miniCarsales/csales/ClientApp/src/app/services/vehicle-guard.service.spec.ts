import { TestBed } from '@angular/core/testing';

import { DeactivateCreateVehicleGuard } from './vehicle-guard.service';

describe('VehicleGuardService', () => {
  let service: DeactivateCreateVehicleGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeactivateCreateVehicleGuard);
  });
});
