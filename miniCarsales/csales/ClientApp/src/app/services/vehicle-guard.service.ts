import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CreateVehicleComponent } from '../create-vehicle/create-vehicle.component';

@Injectable({
  providedIn: 'root'
})
export class DeactivateCreateVehicleGuard implements CanDeactivate<CreateVehicleComponent> {
  constructor() { }

  async canDeactivate(
    component: CreateVehicleComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot
  ): Promise<boolean> {
    const canDeactivate = await component.canDeactivate();
    return canDeactivate;
  }
}
