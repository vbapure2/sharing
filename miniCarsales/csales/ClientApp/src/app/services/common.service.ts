import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action: string, isSuccess: boolean = true) {
    var panelClass = isSuccess ? 'snack-success' : 'snack-error';
    this.snackBar.open(message, action, {
        duration: 10000,
        verticalPosition: 'top',
        panelClass: [panelClass]
    });
}
}
