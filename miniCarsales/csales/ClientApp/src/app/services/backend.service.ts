import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CarModel } from '../models/car-model';
import { ResponseModel } from '../models/response-model';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  baseUrl: string;
  constructor(private httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  public getVehicles(vehicleType: string) {
    return this.httpClient.get<ResponseModel>(`${this.baseUrl}api/vehicle/getVehicles/${vehicleType}`);
  }

  public createCar(carModel: CarModel) {
    return this.httpClient.post<ResponseModel>(`${this.baseUrl}api/vehicle/createCar`, carModel);
  }

}
