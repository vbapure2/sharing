export class BaseViewModel {
    id: number;
    make: string;
    model: string;
    type: string;
}
