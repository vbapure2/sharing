export class ResponseModel {
    message: string;
    isSuccess: boolean;
    returnObject: Object;
}
