import { BaseViewModel } from "./base-model";

export class CarViewModel {
    engine: string;
    doors: number;
    wheels: number;
    bodyType: string;
}

export interface CarModel extends BaseViewModel, CarViewModel {};
