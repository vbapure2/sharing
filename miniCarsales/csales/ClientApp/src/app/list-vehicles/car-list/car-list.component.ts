import { Component, OnInit } from '@angular/core';
import { BackendService } from 'src/app/services/backend.service';
import { VehicleTypes } from 'src/app/models/vehicle-type';
import { CarModel } from 'src/app/models/car-model';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss']
})
export class CarListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'type', 'make', 'model', 'engine', 'doors', 'wheels', 'bodyType'];
  cars: CarModel[];
  processing: boolean;

  constructor(private backendService: BackendService, 
              private commonService: CommonService) { }

  ngOnInit(): void {
    this.cars = [];

    this.processing = true;
    this.backendService
        .getVehicles(VehicleTypes.CAR)
        .subscribe((response) => {

          this.commonService.openSnackBar(response.message, "Ok", response.isSuccess);

          if(response.isSuccess) {
            const carsArr: Array<CarModel> = Object.keys(response.returnObject).map(
              (key: string): CarModel => response.returnObject[key]
            );

            this.cars = [...carsArr];
          }
        }, 
        (error) => {
          this.commonService.openSnackBar("Failed to get cars. Please Contact Administrator", "Ok", false);
          this.processing = false;
        }, () => {
          this.processing = false;
        })
  }

}
