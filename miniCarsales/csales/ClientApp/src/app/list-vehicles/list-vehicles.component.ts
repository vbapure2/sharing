import { Component, OnInit } from '@angular/core';
import { VehicleTypes } from '../models/vehicle-type';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-vehicles',
  templateUrl: './list-vehicles.component.html',
  styleUrls: ['./list-vehicles.component.scss']
})
export class ListVehiclesComponent implements OnInit {
  vehicleTypes: string [];
  selectedVehicleType: string;
  allvehicleTypes = VehicleTypes;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.selectedVehicleType = null;

    if(this.activatedRoute.snapshot.queryParams 
      && this.activatedRoute.snapshot.queryParams.vehicleType) {
        this.selectedVehicleType = this.activatedRoute.snapshot.queryParams.vehicleType.toString();
    }

    this.vehicleTypes = Object.values(VehicleTypes);

  }

  OnVehicleTypeChange($event: any) {
    this.selectedVehicleType = $event;
  }

}
