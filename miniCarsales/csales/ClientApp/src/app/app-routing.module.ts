import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreateVehicleComponent } from './create-vehicle/create-vehicle.component';
import { DeactivateCreateVehicleGuard } from './services/vehicle-guard.service';
import { ListVehiclesComponent } from './list-vehicles/list-vehicles.component';

const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { 
        path: 'create-vehicle', 
        canDeactivate: [DeactivateCreateVehicleGuard],
        component: CreateVehicleComponent 
    },
    { path: 'list-vehicles', component: ListVehiclesComponent}
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
