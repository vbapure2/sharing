import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonFieldsComponent } from './components/common-fields/common-fields.component';
import { CarFieldsComponent } from './components/car-fields/car-fields.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { BackendService } from '../services/backend.service';
import { CommonService } from '../services/common.service';
import { VehicleTypes } from '../models/vehicle-type';

const CARVEHICLETYPE = "Car";

@Component({
  selector: 'app-create-vehicle',
  templateUrl: './create-vehicle.component.html',
  styleUrls: ['./create-vehicle.component.scss']
})
export class CreateVehicleComponent implements OnInit, AfterViewInit {
  vehicleType: string;
  areFormsValid: boolean;
  isVehicleCreated: boolean;
  vehicleTypes = VehicleTypes;
  processing: boolean;

  @ViewChild('commonFields', { read: CommonFieldsComponent }) commonFieldsComponent: CommonFieldsComponent;
  @ViewChild('carFields', { read: CarFieldsComponent }) carFieldsComponent: CarFieldsComponent;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private backendService: BackendService,
    private commonService: CommonService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.processing = false;
    this.areFormsValid = false;
    this.isVehicleCreated = false;
    this.vehicleType = this.activatedRoute.snapshot.queryParams.vehicleType.toString();
  }

  ngAfterViewInit() {
    this.commonFieldsComponent.commonFieldsForm.valueChanges.subscribe((values) => {
      this.areFormsValid = this.commonFieldsComponent.commonFieldsForm.valid && this.carFieldsComponent.carFieldsForm.valid;
    })

    this.carFieldsComponent.carFieldsForm.valueChanges.subscribe((values) => {
      this.areFormsValid = this.commonFieldsComponent.commonFieldsForm.valid && this.carFieldsComponent.carFieldsForm.valid;
    })

    this.canDeactivate()

  }

  canDeactivate(): Promise<boolean> {
    if (this.isVehicleCreated) {
      return Promise.resolve(true);
    }

    if ((this.commonFieldsComponent.commonFieldsForm.touched
      && this.commonFieldsComponent.commonFieldsForm.dirty)
      || (this.carFieldsComponent.carFieldsForm.touched
        && this.carFieldsComponent.carFieldsForm.dirty)) {

      return new Promise((resolve, reject) => {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
          width: '25vw',
          height: '20vh'
        });

        dialogRef.afterClosed().subscribe(result => {
          resolve(result ? true : false);
        });
      })
    } else {
      return Promise.resolve(true);
    }
  }

  navigateToHome() {
    this.router.navigateByUrl("");
  }

  onCarSave() {
    this.isVehicleCreated = false;

    if (this.vehicleType == VehicleTypes.CAR) {
      let carModel = {
        ...this.commonFieldsComponent.commonFieldsForm.value,
        ...this.carFieldsComponent.carFieldsForm.value
      };

      carModel.doors = +carModel.doors;
      carModel.wheels = +carModel.wheels;

      this.processing = true;
      this.backendService.createCar(carModel).subscribe((result) => {
        this.commonService.openSnackBar(result.message, "Ok", result.isSuccess);
        if (result.isSuccess) {
          this.router.navigate(["list-vehicles"], {queryParams: {vehicleType: this.vehicleType}});
          this.isVehicleCreated = true;
        }
      }, (error) => {
        this.processing = false;
        this.commonService.openSnackBar("An Error has Occured. Please Contact Administrator", "Ok", false);
      }, () => {
        this.processing = false;
      });
    }
  }

}
