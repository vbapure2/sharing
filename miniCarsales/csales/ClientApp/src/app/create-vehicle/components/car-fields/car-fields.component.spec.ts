import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarFieldsComponent } from './car-fields.component';
import { FormBuilder } from '@angular/forms';

describe('CarFieldsComponent', () => {
  let component: CarFieldsComponent;
  let fixture: ComponentFixture<CarFieldsComponent>;
  let formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarFieldsComponent ],
      providers: [
        { provide: FormBuilder, useValue: formBuilder }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Car Fields Doors Validation', () => {
    let doors = component.carFieldControls['doors'];

    doors.setValue(null);
    expect(doors.hasError('required')).toBeTruthy();

    doors.setValue("Car");
    expect(doors.hasError('pattern')).toBeTruthy();

    doors.setValue(4);
    expect(doors.valid).toBeTruthy();
  });

  it('Car Fields Wheels Validation', () => {
    let wheels = component.carFieldControls['wheels'];
    wheels.setValue(null);
    expect(wheels.hasError('required')).toBeTruthy();

    wheels.setValue("Car");
    expect(wheels.hasError('pattern')).toBeTruthy();

    wheels.setValue(4.5);
    expect(wheels.valid).toBeFalsy();

    wheels.setValue(4);
    expect(wheels.valid).toBeTruthy();
  });
});
