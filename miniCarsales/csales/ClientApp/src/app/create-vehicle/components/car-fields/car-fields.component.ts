import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CarViewModel } from 'src/app/models/car-model';

@Component({
  selector: 'app-car-fields',
  templateUrl: './car-fields.component.html',
  styleUrls: ['./car-fields.component.scss']
})
export class CarFieldsComponent implements OnInit {
  carFieldsForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.carFieldsForm = this.fb.group({
      engine: ['', Validators.required],
      doors: ['', [Validators.required, Validators.pattern('[0-9]')]],
      wheels: ['', [Validators.required, Validators.pattern('[0-9]')]],
      bodyType: ['', Validators.required],
    });

  }

  get carFieldControls() {
    return this.carFieldsForm.controls;
  }

}
