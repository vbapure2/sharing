import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonFieldsComponent } from './common-fields.component';
import { FormBuilder } from '@angular/forms';

describe('CommonFieldsComponent', () => {
  let component: CommonFieldsComponent;
  let fixture: ComponentFixture<CommonFieldsComponent>;
  let formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommonFieldsComponent],
      providers: [
        { provide: FormBuilder, useValue: formBuilder }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('Common Fields Type Validation', () => {
    let type = component.commonFieldsForm.controls['type'];
    type.setValue("Car");
    expect(type.valid).toBeTruthy();

    type.setValue(null);
    expect(type.hasError('required')).toBeTruthy();
  });

  it('Common Fields Make Validation', () => {
    let make = component.commonFieldsForm.controls['make'];
    make.setValue("test make");
    expect(make.valid).toBeTruthy();

    make.setValue(null);
    expect(make.hasError('required')).toBeTruthy();
  });
});
