import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseViewModel } from 'src/app/models/base-model';
import { validateHorizontalPosition } from '@angular/cdk/overlay';
import { VehicleTypes } from 'src/app/models/vehicle-type';

@Component({
  selector: 'app-common-fields',
  templateUrl: './common-fields.component.html',
  styleUrls: ['./common-fields.component.css']
})
export class CommonFieldsComponent implements OnInit {
  @Input() vehicleType: string;
  commonFieldsForm: FormGroup;
  baseViewModel: BaseViewModel;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.baseViewModel = new BaseViewModel();
    this.baseViewModel.type = this.vehicleType;

    this.commonFieldsForm = this.fb.group({
      id: [''],
      type: [this.baseViewModel.type, Validators.required],
      make: ['', Validators.required],
      model: ['', Validators.required]
    });
  }

}
