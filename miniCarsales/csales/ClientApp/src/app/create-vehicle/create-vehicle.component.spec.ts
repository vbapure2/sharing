import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Location } from "@angular/common";
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from "@angular/platform-browser-dynamic/testing";

import { CreateVehicleComponent } from './create-vehicle.component';
import { BackendService } from '../services/backend.service';
import { of } from 'rxjs';
import { CarModel } from '../models/car-model';
import { CommonService } from '../services/common.service';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { CommonFieldsComponent } from './components/common-fields/common-fields.component';
import { CarFieldsComponent } from './components/car-fields/car-fields.component';
import { By } from '@angular/platform-browser';

describe('CreateVehicleComponent', () => {
  let component: CreateVehicleComponent;
  let fixture: ComponentFixture<CreateVehicleComponent>;
  let commonFieldsFixture: ComponentFixture<CommonFieldsComponent>;
  let formBuilder: FormBuilder = new FormBuilder();
  let location: Location;

  let httpClientSpy: { post: jasmine.Spy };
  httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
  let backendService = new BackendService(<any>httpClientSpy, "http://localhost:4200");

  let matSnackBarSpy: { post: jasmine.Spy };
  matSnackBarSpy = jasmine.createSpyObj('MatSnackBar', ['open']);
  let commonService = new CommonService(<any>matSnackBarSpy);

  let expectedResponse = { isSuccess: true, message: "test", returnObject: null };
  httpClientSpy.post.and.returnValue(of(expectedResponse));

  let dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of({}), close: null });
  dialogRefSpyObj.componentInstance = { body: '' };
  let dialogSpy = jasmine.createSpyObj('MatDialog', ['open']);
  dialogSpy.open.and.returnValue(dialogRefSpyObj);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, BrowserDynamicTestingModule, ReactiveFormsModule],
      providers: [
        { provide: BackendService, useValue: backendService },
        { provide: CommonService, useValue: commonService },
        { provide: MatDialog, useValue: dialogSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              queryParams: {
                vehicleType: 'Car',
              },
            }
          }
        },
        { provide: FormBuilder, useValue: formBuilder }
      ],
      declarations: [CreateVehicleComponent, CommonFieldsComponent, CarFieldsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateVehicleComponent);
    component = fixture.componentInstance;

    commonFieldsFixture = TestBed.createComponent(CommonFieldsComponent);
    component.commonFieldsComponent = commonFieldsFixture.componentInstance;

    location = TestBed.get(Location);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onCarSave', () => {
    fixture.componentInstance.onCarSave();
    expect(component.isVehicleCreated).toEqual(true);
    expect(location.path()).toBe('');
  });

  it('canDeactivate - Is Car Already Created', async () => {
    fixture.componentInstance.isVehicleCreated = true;
    const canDeactivate = await fixture.componentInstance.canDeactivate();
    expect(canDeactivate).toEqual(true);
  });

  it('canDeactivate - Are Fields touched - User Did not Confirm', async () => {
    let dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of(null), close: null });
    dialogRefSpyObj.componentInstance = { body: '' };

    let matDialogSpy = TestBed.get(MatDialog);
    matDialogSpy.open.and.returnValue(dialogRefSpyObj);

    fixture.componentInstance.commonFieldsComponent.commonFieldsForm.markAsTouched();
    fixture.componentInstance.commonFieldsComponent.commonFieldsForm.markAsDirty();

    const canDeactivate = await fixture.componentInstance.canDeactivate();
    expect(canDeactivate).toEqual(false);
  });

  it('canDeactivate - Are Fields touched - User Confirmed', async () => {
    let dialogRefSpyObj = jasmine.createSpyObj({ afterClosed: of(true), close: null });
    dialogRefSpyObj.componentInstance = { body: '' };

    let matDialogSpy = TestBed.get(MatDialog);
    matDialogSpy.open.and.returnValue(dialogRefSpyObj);

    fixture.componentInstance.commonFieldsComponent.commonFieldsForm.markAsTouched();
    fixture.componentInstance.commonFieldsComponent.commonFieldsForm.markAsDirty();

    const canDeactivate = await fixture.componentInstance.canDeactivate();
    expect(canDeactivate).toEqual(true);
  });

});
