import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VehicleTypes } from '../models/vehicle-type';

const VEHICLETYPECAR = "Car";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  vehicleTypes: string [];
  selectedVehicleType: string;

  constructor(private router: Router) {}
  
  ngOnInit() {
    this.vehicleTypes = Object.values(VehicleTypes);
    this.selectedVehicleType = null;
  }

  OnVehicleTypeChange($event: any) {
    console.log('selected vehicle type: ', $event);
    this.selectedVehicleType = $event;

    this.router.navigate(['create-vehicle'], {queryParams: {vehicleType: $event}});
  
  }
}
