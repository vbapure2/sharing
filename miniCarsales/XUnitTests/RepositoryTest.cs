using System.Collections.Generic;
using System.Linq;
using AutoFixture.Xunit2;
using csales.Models;
using csales.Models.Constants;
using csales.Repositories;
using Xunit;

namespace XUnitTests
{

    public class RepositoryTest: IClassFixture<SharedDatabaseFixture>
    {
        public SharedDatabaseFixture Fixture { get; }
        public RepositoryTest(SharedDatabaseFixture fixture) => Fixture = fixture;


        [Fact]
        public void GetVehicles_Cars_Not_Found1()
        {
            using (var context = Fixture.CreateContext())
            {

                var commonRepo = new CommonRepository(context);

                var expectedSuccess = false;
                object expectedCars = null;
                var response = commonRepo.GetVehicles(VehicleTypes.CAR);

                Assert.Equal(response.IsSuccess, expectedSuccess);
                Assert.Equal(response.ReturnObject, expectedCars);
            }
        }

        [Fact]
        public void GetVehicles_Cars_Found2()
        {
            using (var context = Fixture.CreateContext())
            {
                var carModel = new CarModel();
                var id = 100;
                var carRepo = new CarRepository(context);

                carModel.Id = id;
                carModel.Type = VehicleTypes.CAR;
                var response = carRepo.CreateCar(carModel);

                carModel.Id = id + 1;
                carModel.Type = VehicleTypes.CAR;
                response = carRepo.CreateCar(carModel);

                var commonRepo = new CommonRepository(context);

                var actualCars = response.ReturnObject;

                var expectedSuccess = true;
                response = commonRepo.GetVehicles(VehicleTypes.CAR);
                Assert.Equal(response.IsSuccess, expectedSuccess);
            }
        }

        [Theory]
        [InlineAutoData(10, "Car", true)]
        [InlineAutoData(11, "Car", true)]
        [InlineAutoData(11, "Car", false)]
        [InlineAutoData(12, "Car", true)]
        public void CreateCar3(int id, string vehicleType, bool expected, CarModel carModel)
        {
            using (var context = Fixture.CreateContext())
            {
                var carRepo = new CarRepository(context);

                carModel.Id = id;
                carModel.Type = vehicleType;

                var response = carRepo.CreateCar(carModel);

                Assert.Equal(response.IsSuccess, expected);
            }
        }

    }
}
