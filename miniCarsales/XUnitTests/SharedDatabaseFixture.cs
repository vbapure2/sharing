﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using csales.Repositories;
using csales.Repositories.Entities;
using Microsoft.EntityFrameworkCore;

namespace XUnitTests
{
    public class SharedDatabaseFixture
    {
        private static readonly object _lock = new object();
        private static bool _databaseInitialized;

        public SharedDatabaseFixture()
        {
            Seed();
        }

        public VehicleDbContext CreateContext(DbTransaction transaction = null)
        {
           var context = new VehicleDbContext(new DbContextOptionsBuilder<VehicleDbContext>().UseInMemoryDatabase("Vehicles").Options);
           return context;
        }

        private void Seed()
        {
            lock (_lock)
            {
                if (!_databaseInitialized)
                {
                    using (var context = CreateContext())
                    {
                        context.Database.EnsureDeleted();
                        context.Database.EnsureCreated();
                    }

                    _databaseInitialized = true;
                }
            }
        }

    }
}
