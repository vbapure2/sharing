﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace s3test
{
    class Program
    {
        static void Main(string[] args)
        {
            // Installed - System.Configuration.ConfigurationManager
            using (var s3Client = new AmazonS3Client(ConfigurationManager.AppSettings.Get("awskey"),
                ConfigurationManager.AppSettings.Get("awssecret"), 
                Amazon.RegionEndpoint.USEast1))
            {
                GetObjectRequest request = new GetObjectRequest();
                request.BucketName = "vbapure2";
                request.Key = "students.txt";
                GetObjectResponse response = s3Client.GetObject(request);
                Stream responseStream = response.ResponseStream;

                using (StreamReader reader = new StreamReader(responseStream))
                {
                    var res = reader.ReadToEnd();
                    var result = JsonConvert.DeserializeObject<Students>(res);
                
                    Console.WriteLine(result);    
                }

                Console.ReadLine();
            }
        }

        public class Students
        {

            public List<Student> students { get; set; }
        }

        public class Student
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public string DoB { get; set; }

            public int Flag { get; set; }

            public int Gender { get; set; }
        }
       
    }
}
