﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Business
{
    public interface IDomainService
    {
        ICollection<Student> GetStudentsFromPersistence();
        void UpdatePersistence(Students students);
        Students ReadS3();
    }

    public class DomainService: IDomainService
    {
        private IAWSS3Reader _awss3Reader;
        private IStudentRepository _studentsRepository;

        public DomainService(IAWSS3Reader awss3Reader, IStudentRepository studentsRepository)
        {
            _awss3Reader = awss3Reader;
            _studentsRepository = studentsRepository;

            //UpdatePersistence(ReadS3());
        }

        public Students ReadS3()
        {
            return _awss3Reader.ReadS3();
        }

        public void UpdatePersistence(Students students)
        {
            _studentsRepository.UpsertStudents(students);
            return;
        }

        public ICollection<Student> GetStudentsFromPersistence()
        {
            return _studentsRepository.GetAllStudents();
        }
    }
}