﻿using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Business
{
    public interface IAWSS3Reader
    {
        Students ReadS3();
    }

    public class AWSS3Reader: IAWSS3Reader
    {
        public AWSS3Reader()
        {
        }

        public Students ReadS3()
        {
            using (var s3Client = new AmazonS3Client(ConfigurationManager.AppSettings.Get("awskey"),
               ConfigurationManager.AppSettings.Get("awssecret"),
               Amazon.RegionEndpoint.USEast1))
            {
                GetObjectRequest request = new GetObjectRequest();
                request.BucketName = ConfigurationManager.AppSettings.Get("awsbucketName");
                request.Key = ConfigurationManager.AppSettings.Get("awsobjectKey");

                GetObjectResponse response = s3Client.GetObject(request);
                Stream responseStream = response.ResponseStream;

                using (StreamReader reader = new StreamReader(responseStream))
                {
                    var res = reader.ReadToEnd();
                    var result = JsonConvert.DeserializeObject<Students>(res);

                    return result;
                 }
            }
        }
    }
}