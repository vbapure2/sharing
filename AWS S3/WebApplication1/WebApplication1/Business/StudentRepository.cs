﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication1.Context;
using WebApplication1.Models;

namespace WebApplication1.Business
{
    public interface IStudentRepository
    {
        void UpsertStudents(Students modelList);
        ICollection<Student> GetAllStudents();
        IQueryable<Student> GetStudent(int studentId);
    }

    public class StudentRepository: IStudentRepository
    {
        private StudentContext _studentsContext;
        public StudentRepository(StudentContext studentsContext)
        {
            _studentsContext = studentsContext;
        }

        public void UpsertStudents(Students modelList)
        {
            foreach(var stud in modelList.students)
            {
                if(GetStudent(stud.StudentId).Count() == 0)
                {
                    _studentsContext.Students.Add(stud);
                }
            }

            _studentsContext.SaveChanges();
        }

        public ICollection<Student> GetAllStudents()
        {
           return  _studentsContext.Students.ToList();
        }

        public IQueryable<Student> GetStudent(int studentId)
        {
            return _studentsContext.Students.Where(_ => _.StudentId == studentId);
        }
    }
}