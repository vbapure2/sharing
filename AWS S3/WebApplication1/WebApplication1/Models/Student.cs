﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Students
    {
        public List<Student> students { get; set; }
    }

    public class Student
    {
        [Key]
        public int Id { get; set; }
        public int StudentId { get; set; }
        public string Name { get; set; }
        public DateTime DoB { get; set; }
        public Gender Gender { get; set; }
        public int Flag { get; set; }
    }

    public enum Gender
    {
        Male, Female, Other
    }
}