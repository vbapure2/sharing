﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication1.Business;
using WebApplication1.Context;

namespace WebApplication1
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer(new DatabaseInitializer());

            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;

            builder
                .RegisterType<StudentContext>()
                .InstancePerRequest();

            builder
                .RegisterType<StudentRepository>()
                .As<IStudentRepository> ();

            builder.RegisterType<AWSS3Reader>()
                    .As<IAWSS3Reader>()
                    .InstancePerRequest();

            builder.RegisterType<DomainService>()
                    .As<IDomainService>()
                    .InstancePerRequest();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
