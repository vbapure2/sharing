﻿using Amazon.S3;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApplication1.Business;
using WebApplication1.Context;
using WebApplication1.Filters;

namespace WebApplication1.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [NotHandledExceptionAttibute]
    public class StudentsController : ApiController
    {
        private IDomainService _domainService;
        //public StudentsController()
        //{
        //    _domainService = new DomainService(new AWSS3Reader(),
        //                                   new StudentRepository(new StudentContext()));  
        //}

        public StudentsController(IDomainService domainService)
        {
            _domainService = domainService;
        }

        public HttpResponseMessage Get()
        {
            try
            {
                _domainService.UpdatePersistence(_domainService.ReadS3());

                var studentslist = _domainService.GetStudentsFromPersistence();

                return Request.CreateResponse(HttpStatusCode.OK, studentslist);
            }
            catch (JsonReaderException jex)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Non Amazing Error");
                return response;
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                var response = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Amazon Error");
                return response;
            }
        }        
            
    }
}